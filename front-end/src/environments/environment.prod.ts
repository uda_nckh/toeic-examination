export const environment = {
  production: true,
  apiUrl: `http://localhost:4000`,
  application:
  {
    name: 'angular-starter',
    angular: 'Angular 11.0.2',
    bootstrap: 'Bootstrap 4.5.3',
    fontawesome: 'Font Awesome 5.15.1',
  },
  toeic_Api:`http://localhost:8001`,
};
