import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApiService } from '../common/api-service/api.service';
import { SEOService } from 'src/app/common/api-service/seo.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { SendDataService } from '../common/api-service/send-data.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { MustMatch } from '../common/validations/must-match.validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  subscription: Subscription[] = [];

  inputOrderMenu: any = {
    Fullname: '',
    Email: '',
    Password: '',
    
  };

  // menu
  isShow: boolean = false;
  isRegister: boolean = false;
  info: any = null;

  // data binding
  headerInfo = {
    Phone: '',
    Mail: '',
    mailHref: '',
    phoneHref: '',
  };

  dataFooter = {
    AndroidLink: '',
    IosLink: '',
  };

  inputLogin: any = {
    Email: '',
    Password: '',
  };

  inputRegister: any = {
    Name: '',
    Email: '',
    Password: '',
  };

  dataMenu: any;
  idUser: string;
  infoUser: any = null;
  userValue: any = [];
  amount: number = 0;
  CityDatas: any[] = [];
  DistrictDatas: any[] = [];
  shopMenuCatgores: any[] = [];
  shopMenuMobile: any[] = [];
  idCustomer: any;
  // binding logo
  logo: string = '';
  avatar: string = '';

  getUrl: any;

  // url Current
  urlCurrent: string = '';

  listCateRecipe: any;
  isMobile: boolean = false;
  isShowMenuMobile: boolean = false;
  isCheckEmail: boolean = false;
  isDisabledEmail: boolean = false;
  isClassShow: boolean = false;
  isClass1Show: boolean = false;
  //form
  form: FormGroup;
  form1: FormGroup;
  form2: FormGroup;

  @ViewChild('updateInformation') updateInformation: ElementRef;
  @ViewChild('login') login: ElementRef;

  /**
   * constructor
   * @param api
   */
  constructor(
    public api: ApiService,
    public auth: AngularFireAuth, // Inject Firebase auth service
    private router: Router,
    private sendDataService: SendDataService,
    private seoService: SEOService,
    private formBuilder: FormBuilder,
    private formBuilder1: FormBuilder
  ) {
    this.getUrl = localStorage.getItem('lastUrl');
    // add validate for controls
    this.form = this.formBuilder.group({
      Name: ['', [Validators.required]],

      Email: ['', [Validators.required, Validators.email]],
      
    });

    this.form1 = this.formBuilder.group(
      {
        password: ['', [Validators.required, Validators.minLength(6)]],
        repassword: ['', [Validators.required]],
      },
      { validator: MustMatch('password', 'repassword') }
    );

    this.form2 = this.formBuilder.group({
      Email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
    this.isMobileDevice();
    // url Current
    this.urlCurrent = this.api.getUrl();

    let title = '';
    if (this.urlCurrent.indexOf('/en/') == -1) {
      title = 'Đăng nhập';
    } else {
      title = 'Login';
    }

    //Seo title
    this.seoService.setTitle(title);

    this.idUser = this.api.getStaffValue?.id;
    this.userValue = this.api.getStaffValue;

    if (this.userValue != null) {
      this.infoUser = this.userValue;

      // set avatar default user
      if (this.infoUser['Avatar'] == '' || this.infoUser['Avatar'] == null) {
        this.infoUser['Avatar'] =
          'https://w7.pngwing.com/pngs/419/473/png-transparent-computer-icons-user-profile-login-user-heroes-sphere-black-thumbnail.png';
      } else {
        this.infoUser['Avatar'] = this.infoUser['Avatar'];
      }
    }
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * isMobileDevice
   */
  isMobileDevice() {
    var userAgent = navigator.userAgent;
    if (
      /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        userAgent
      )
    ) {
      // true for mobile device
      this.isMobile = true;
    } else {
      // false for not mobile device
      this.isMobile = false;
    }
  }

  //log out candidate
  logOut() {
    this.api.logoutStaff();
    location.reload();
  }

  onCheckEmail(Email) {
    if (!this.isDisabledEmail) {
      const param = {
        Email: Email,
      };

      this.subscription.push(
        this.api.excuteAllByWhat(param, '3114', true).subscribe((result) => {
          let data = result.data;
          if (result.status == true && data.length > 0) {
            this.isCheckEmail = true;
          } else {
            this.isCheckEmail = false;
          }
        })
      );
    }
  }

  /**
   * onBtnSummit
   */
   Register() {
    this.api.register(this.inputOrderMenu.Fullname, this.inputOrderMenu.Email, this.inputOrderMenu.Password).subscribe((result) => {
      
      this.api.showSuccess('Đăng ký thành công');
      window.location.reload();
    });

    // let what = '3122';
    // if (this.isRegister) {
    //   what = '3124';
    // }
    // if (!this.isRegister) {
    //   if (!this.form.invalid) {
    //     this.subscription.push(
    //       this.api
    //         .excuteAllByWhat(this.inputOrderMenu, what, true)
    //         .subscribe((result) => {
    //           let data = result.result_select.data;
    //           if (
    //             result.status == true &&
    //             result.insert == true &&
    //             data.length > 0
    //           ) {
    //             localStorage.setItem('staffSubject', JSON.stringify(data[0]));
    //             this.api.staffSubject.next(data[0]);
    //             this.api.showSuccess('Đăng nhập thành công');
    //             //redirect to last page
    //             if (this.getUrl == '' || this.getUrl == undefined) {
    //               this.router.navigate(['/']);
    //             } else {
    //               this.router.navigate([this.getUrl]);
    //             }
    //             this.form.reset();
    //           } else {
    //             setTimeout(() => {
    //               this.api.showError(' Vui lòng đăng ký lại');
    //               this.updateInformation.nativeElement.click();
    //             }, 1000);
    //           }
    //         })
    //     );
    //   } else {
    //     setTimeout(() => {
    //       this.api.showError('Vui lòng điền đầy đủ thông tin để đăng ký');
    //       this.updateInformation.nativeElement.click();
    //     }, 1000);
    //   }
    // } else {
    //   if (!this.form.invalid && !this.form1.invalid && !this.isCheckEmail) {
    //     this.subscription.push(
    //       this.api
    //         .excuteAllByWhat(this.inputOrderMenu, what, true)
    //         .subscribe((result) => {
    //           let data = result.result_select.data;
    //           if (
    //             result.status == true &&
    //             result.insert == true &&
    //             data.length > 0
    //           ) {
    //             localStorage.setItem('staffSubject', JSON.stringify(data[0]));
    //             this.api.staffSubject.next(data[0]);
    //             this.api.showSuccess('Đăng nhập thành công');
    //             //redirect to last page
    //             if (this.getUrl == '' || this.getUrl == undefined) {
    //               this.router.navigate(['/']);
    //             } else {
    //               this.router.navigate([this.getUrl]);
    //             }
    //             this.form.reset();
    //             this.form1.reset();
    //           } else {
    //             setTimeout(() => {
    //               this.api.showError(' Vui lòng đăng ký lại');
    //               this.updateInformation.nativeElement.click();
    //             }, 1000);
    //           }
    //         })
    //     );
    //   } else {
    //     setTimeout(() => {
    //       this.api.showError('Vui lòng điền đầy đủ thông tin để đăng ký');
    //       this.updateInformation.nativeElement.click();
    //     }, 1000);
    //   }
    // }
  }

  /**
   * btnLogin
   */
  btnLogin() {

    this.api.excuteLogin(this.inputLogin.Email, this.inputLogin.Password).subscribe((result) => {
  
      localStorage.setItem('staffSubject', JSON.stringify(result['user_info']));
      this.api.staffSubject.next(result['user_info']);
      this.router.navigate(['/']);

      this.api.showSuccess('Đăng nhập thành công');
    });

    // const param = {
    //   Email: this.inputLogin.Email,
    //   Password: this.inputLogin.Password,
    // };
    // if (!this.form2.invalid) {
    //   this.subscription.push(
    //     this.api.excuteAllByWhat(param, '3115', true).subscribe((result) => {
    //       let data = result.data;
    //       if (result.status == true && data.length > 0) {
    //         localStorage.setItem('staffSubject', JSON.stringify(data[0]));
    //         this.api.staffSubject.next(data[0]);
    //         this.api.showSuccess('Đăng nhập thành công');
    //         //redirect to last page
    //         if (this.getUrl == '' || this.getUrl == undefined) {
    //           this.router.navigate(['/']);
    //         } else {
    //           this.router.navigate([this.getUrl]);
    //         }
    //       } else {
    //         setTimeout(() => {
    //           this.api.showError(' Mật khẩu hoặc Email chưa đúng');
    //           this.login.nativeElement.click();
    //         }, 1000);
    //       }
    //     })
    //   );
    // } else {
    //   setTimeout(() => {
    //     this.api.showError('Vui lòng nhập đầy đủ thông tin');
    //     this.login.nativeElement.click();
    //   }, 1000);
    // }
  }
}
