import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';
import {
  Router,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError,
} from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  subscription: Subscription[] = [];
  ads: any = [];
  isShow = true;
  isNone = false;
  title = 'Rolie Viet Nam';
  getUrl: any;
  @ViewChild('openModal') openModal: ElementRef;
  isCheckADS: boolean = false;
  defaultImage: any = "assets/images/loading.gif";

  /**
   * constructor
   * @param api
   */
  constructor(
    public api: ApiService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    // check url change
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        // Show loading indicator
        this.spinner.show();
      }

      if (event instanceof NavigationEnd) {
        // Hide loading indicator
        document
          .getElementById('navbarSupportedContent')
          ?.classList.remove('show');

        /** spinner ends after 1 seconds */
        setTimeout(() => {
          this.spinner.hide();
        }, 500);
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
        console.log(event.error);

        /** spinner ends after 1 seconds */
        setTimeout(() => {
          this.spinner.hide();
        }, 500);
      }
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {    
    setTimeout(() => {
      this.getAds();
    }, 2000);
  }

  /**
   *
   */
  ngAfterViewInit() { 
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * getAds
   */
  getAds() {
    const param = {};
    this.subscription.push(
      this.api.excuteAllByWhat(param, '5500', true).subscribe((result) => {
        let data = result.data;
        if (result.status == true && data.length > 0) {
          this.ads = data[0];
          this.isCheckADS = true;
        } else {
          this.ads = [];
          this.isCheckADS = false;
        }
        if (this.router.url == '/' && this.isCheckADS) {
          this.openModal?.nativeElement.click();
        }
      })
    );
  }

  /**
   * onAdsClick
   * @param link
   */
  onAdsClick(link) {
    const url = '/' + link; 
    this.router.navigate([url.toLowerCase()]);
    
  }

  /***
   * closeModal
   */
  closeModal() { 
    document.getElementById('exampleModalCenter').style.display =
      'none!important';
  }

  navigatePage(item): void {
    this.router.navigate([item]);
  }
}
