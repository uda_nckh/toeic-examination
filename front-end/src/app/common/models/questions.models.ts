export class Question {
  id: string;
  exams_id: string;
  question_index: string;
  question: string;
  anwser_a: string;
  anwser_b: string;
  anwser_c: string;
  anwser_d: string;
  correct: string;
  image_url: string;
  audio: string;
  create_on: string;
}
