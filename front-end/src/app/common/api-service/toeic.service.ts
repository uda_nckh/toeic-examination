import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import {
  HttpHeaders,
  HttpClient,
  HttpErrorResponse,
} from '@angular/common/http';
import { throwError } from 'rxjs/internal/observable/throwError';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Category } from './../models/categories.models';
import { Exam } from '../models/exams.models';
import { Question } from '../models/questions.models';

@Injectable({
  providedIn: 'root',
})

export class ToeicService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // Authorization: 'my-auth-token',
      // Authorization: 'Basic ' + btoa('username:password'),
    }),
  };

  constructor(private httpClient: HttpClient) {}

  //GET
  public getAllCategories() {
    const url = `${environment.toeic_Api}/categories/`;
    return this.httpClient
      .get<any>(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  public getCategory(id: number) {
    const url = `${environment.toeic_Api}/categories/`+id;
    return this.httpClient
      .get<any>(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  public getAllExams() {
    const url = `${environment.toeic_Api}/exams/`;
    return this.httpClient
      .get<any>(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }
  public getAllQuestions() {
    const url = `${environment.toeic_Api}/questions/`;
    return this.httpClient
      .get<any>(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }
  public getQuestionByExamId() {

    const id = Math.floor(Math.random() * 6) + 1;
    const url = `${environment.toeic_Api}/questions/${id}/`;
    return this.httpClient
      .get<any>(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  // public getAllUsers() {
  //   const url = `${environment.toeic_Api}/questions/`;
  //   return this.httpClient
  //     .get<any>(url, this.httpOptions)
  //     .pipe(catchError(this.handleError));
  // }
    // public getAllCandidates() {
  //   const url = `${environment.toeic_Api}/questions/`;
  //   return this.httpClient
  //     .get<any>(url, this.httpOptions)
  //     .pipe(catchError(this.handleError));
  // }

  //POST
  public addCategory(data: Category) {
    const url = `${environment.toeic_Api}/categories`;
    return this.httpClient
      .post<any>(url, data, this.httpOptions)
      .pipe(catchError(this.handleError));
  }
  public addExam(data: Exam) {
    const url = `${environment.toeic_Api}/exams`;
    return this.httpClient
      .post<any>(url, data, this.httpOptions)
      .pipe(catchError(this.handleError));
  }
  public addQuestion(data: Question) {
    const url = `${environment.toeic_Api}/questions/`;
    return this.httpClient
      .post<any>(url, data, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  //EDIT
  public editCategory(id: number, data: Category){
    const url = `${environment.toeic_Api}/update_category?id=`+ id;
    return this.httpClient
    .put<any>(url, data, this.httpOptions)
    .pipe(catchError(this.handleError));
  }
  public editExam(id: number, data: Exam){
    const url = `${environment.toeic_Api}/update_exam?id=`+ id;
    return this.httpClient
    .put<any>(url, data, this.httpOptions)
    .pipe(catchError(this.handleError));
  }
  public editQuestion(id: number, data: Question){
    const url = `${environment.toeic_Api}/update_exam?id=`+ id;
    return this.httpClient
    .put<any>(url, data, this.httpOptions)
    .pipe(catchError(this.handleError));
  }

  // DELETE
  public deleteCategory(id: number) {
    const url = `${environment.toeic_Api}/delete_category?id=` + id;
    return this.httpClient.delete<any>(url).pipe(catchError(this.handleError));
  }
  public deleteExam(id: number) {
    const url = `${environment.toeic_Api}/delete_exam?id=` + id;
    return this.httpClient.delete<any>(url).pipe(catchError(this.handleError));
  }
  public deleteQuestion(id: number) {
    const url = `${environment.toeic_Api}/delete_question?id=` + id;
    return this.httpClient.delete<any>(url).pipe(catchError(this.handleError));
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
