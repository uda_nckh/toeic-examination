import { MenuComponent } from 'src/app/home-page-manager/menu/menu.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from 'src/app/home-page-manager/home-page.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { LoginCookie } from '../common/core/login-cookie';
import { AuthGuard } from '../common/_helpers/auth.guard';

@NgModule({
    declarations: [
        HomePageComponent,
        MenuComponent
    ],
    imports: [
        TransferHttpCacheModule,
        CommonModule,
        RouterModule.forChild([
            {
                path: '', component: HomePageComponent, children: [
                  {
                    path: '',
                    loadChildren: () => import('./content/content.module').then(m => m.ContentModule)
                },
                ],
            }
        ]),

    ],
    providers: [LoginCookie]
})
export class HomePageModule { }
