import { NgModule } from '@angular/core';
import { B2CategoryFormComponent } from './b2-category-form.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [B2CategoryFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '', component: B2CategoryFormComponent, children: [
        ],
      }
    ]),
  ]
})
export class B2CategoryFormModule { }
