import { Router } from '@angular/router';
import { ToeicService } from 'src/app/common/api-service/toeic.service';
import {
  AfterViewInit,
  Component,
  ViewChild,
  OnInit,
  Output,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Exam } from 'src/app/common/models/exams.models';
import { Category } from 'src/app/common/models/categories.models'

import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';


// export interface Category {
//   category_id: string;
//   name: string;
//   create_on: string;
// }

// export interface Exam {
//   id: string;
//   category_id: string;
//   name: string;
//   create_on: string;
// }


@Component({
  selector: 'app-c1-exam',
  templateUrl: './c1-exam.component.html',
  styleUrls: ['./c1-exam.component.scss'],
})
export class C1ExamComponent implements AfterViewInit, OnInit {
  public Category: any[]= [];
  ELEMENT_DATA: Exam[] = [];
  value = '';
  closeResult: string;
  selectedValue: string;
  displayedColumns: string[] = [
    'id',
    'category',
    'name',
    'create_on',
    'action',
  ];

  // readonly dataSource = this.ELEMENT_DATA.map(data => ({
  //   id: data.id,
  //   category: this.Category.find(category => category.category_id === data.category_id),
  //   name: data.name,
  //   create_on:data.name
  // }));
  dataSource = new MatTableDataSource<Exam>(this.ELEMENT_DATA);

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;


  constructor(
    private toeicService: ToeicService,
    private httpClient: HttpClient,
    private router: Router,
    private modalService: NgbModal,
    // private fb: FormBuilder
  ) {}
  ngOnInit() {
    this.getExamTable();
    this.toeicService.getAllCategories().subscribe((data) => {
      console.log('getAllCategories', data);
      this.Category = data;
    });
  }

  // addExam() {
  //   this.router.navigate(['b1-category']);
  // }
  getExamTable() {
    let resp = this.toeicService.getAllExams();
    resp.subscribe((report) => (this.dataSource.data = report as Exam[]

    ));
  }
  deleteExam(id) {
    this.toeicService.deleteExam(id).subscribe((data) => {
      console.log('delete', data);
      this.getExamTable();
    });
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter() {
    this.dataSource.filter = this.value.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onSearchClear() {
    this.value = '';
    this.applyFilter();
  }

  addExam(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  public id = 0;
  public examForm = new FormGroup({
    category_id: new FormControl(''),
    name: new FormControl(''),
  });
  // private loadData(id) {
  //   this.toeicService.getAllCategories().subscribe((data) => {
  //     console.log('getCategories', data);
  //     this.ELEMENT_DATA = data;
  //     for (const controlName in this.examForm.controls) {
  //       if (controlName) {
  //         this.examForm.controls[controlName].setValue(data[controlName]);
  //       }
  //     }
  //   });
  // }
  private createNewData() {
    const newExam = {};
    for (const controlName in this.examForm.controls) {
      if (controlName) {
        newExam[controlName] = this.examForm.controls[
          controlName
        ].value;
      }
    }
    return newExam as Exam;
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
  public saveAndGotoList() {
    if (this.id > 0) {
      this.toeicService
        .editExam(this.id, this.createNewData())
        .subscribe((data) => {});
    } else {
      this.toeicService
        .addExam(this.createNewData())
        .subscribe((data) => {});
    }
    this.reloadCurrentRoute();
  }
  public save() {
    if (this.id > 0) {
      this.toeicService
        .editExam(this.id, this.createNewData())
        .subscribe((data) => {
          console.log('update');
        });
    } else {
      this.toeicService.addExam(this.createNewData()).subscribe((data) => {
        this.examForm.reset();
        console.log('re');
      });
    }
    this.reloadCurrentRoute();
  }
}
