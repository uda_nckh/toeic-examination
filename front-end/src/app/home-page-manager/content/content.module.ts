import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';

@NgModule({
  declarations: [ContentComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentComponent,
        children: [
          {
            path: '',
            redirectTo: 'a1-user',
            pathMatch: 'full'
            },
          {
            path: 'a1-user',
            loadChildren: () =>
              import('./a1-user/a1-user.module').then((m) => m.A1UserModule),
          },
          {
            path: 'b1-category',
            loadChildren: () =>
              import('./b1-category/b1-category.module').then(
                (m) => m.B1CategoryModule
              ),
          },
          {
            path: 'b2-category-form',
            loadChildren: () =>
              import('./b2-category-form/b2-category-form.module').then(
                (m) => m.B2CategoryFormModule
              ),
          },

          {
            path: 'c1-exam',
            loadChildren: () =>
              import('./c1-exam/c1-exam.module').then((m) => m.C1ExamModule),
          },
          {
            path: 'd1-question',
            loadChildren: () =>
              import('./d1-question/d1-question.module').then(
                (m) => m.D1QuestionModule
              ),
          },
          {
            path: 'e1-listuser',
            loadChildren: () =>
              import('./e1-list-user/e1-list-user.module').then(
                (m) => m.E1ListUserModule
              ),
          },
        ],
      },
    ]),
  ],
  providers: [],
  entryComponents: [],
})
export class ContentModule {}
