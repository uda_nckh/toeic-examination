import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';



import { B1CategoryComponent } from './b1-category.component';


@NgModule({
  declarations: [B1CategoryComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '', component: B1CategoryComponent, children: [

        ],
      },
      // {
      //   path: 'b2-category-form/:id',
      //   loadChildren: () =>
      //     import('../b2-category-form/b2-category-form.module').then(
      //       (m) => m.B2CategoryFormModule
      //     ),
      // },
    ]),
  ]
})
export class B1CategoryModule { }
