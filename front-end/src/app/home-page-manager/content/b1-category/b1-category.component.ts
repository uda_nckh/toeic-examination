import { HttpClient } from '@angular/common/http';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Category } from './../../../common/models/categories.models';
import { Router } from '@angular/router';

import { ToeicService } from 'src/app/common/api-service/toeic.service';
import {
  AfterViewInit,
  Component,
  ViewChild,
  OnInit,
  Output,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
// import { Category } from 'src/app/common/models/categories.models';
@Component({
  selector: 'app-b1-category',
  templateUrl: './b1-category.component.html',
  styleUrls: ['./b1-category.component.scss'],
})
export class B1CategoryComponent implements AfterViewInit, OnInit {
  public ELEMENT_DATA: Category[] = [];
  value = '';
  displayedColumns: string[] = ['id', 'name', 'create_on', 'action'];
  dataSource = new MatTableDataSource<Category>(this.ELEMENT_DATA);

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;
  closeResult: string;
  editCategoryForm: FormGroup;
  constructor(
    private toeicService: ToeicService,
    private httpClient: HttpClient,
    private router: Router,
    private route: Router,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {}
  ngOnInit() {
    this.getCategoryTable();
    this.editCategoryForm = this.fb.group({
      name: [''],
    } );

  }
  addCategory() {
    // this.router.navigate(['b2-category-form', 0]);
    // this.router.navigateByUrl('/b2-category-form/0');
  }
  getCategory(id: number) {
    this.toeicService.getCategory(id).subscribe((data) => {
      console.log('1cate', data);
      const nameCate = data[id].name
      console.log('name', nameCate)
    });
  }
  getCategoryTable() {
    let resp = this.toeicService.getAllCategories();
    resp.subscribe((report) => (this.dataSource.data = report as Category[]));
  }
  deleteCategory(id: number) {
    this.toeicService.deleteCategory(id).subscribe((data) => {
      console.log('delete', data);
      this.getCategoryTable();
    });
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter() {
    this.dataSource.filter = this.value.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onSearchClear() {
    this.value = '';
    this.applyFilter();
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public id = 0;
  public categoryForm = new FormGroup({
    name: new FormControl(''),
  });
  // private loadData(id) {
  //   this.toeicService.getAllCategories().subscribe((data) => {
  //     console.log('getCategories', data);
  //     this.ELEMENT_DATA = data;
  //     for (const controlName in this.categoryForm.controls) {
  //       if (controlName) {
  //         this.categoryForm.controls[controlName].setValue(data[controlName]);
  //       }
  //     }
  //   });
  // }
  private createNewData() {
    const newCategory = {};
    for (const controlName in this.categoryForm.controls) {
      if (controlName) {
        newCategory[controlName] = this.categoryForm.controls[
          controlName
        ].value;
      }
    }
    return newCategory as Category;
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
  public saveAndGotoList() {
    // if (this.id > 0) {
    //   this.toeicService
    //     .editCategory(this.id, this.createNewData())
    //     .subscribe((data) => {});
    // } else {

    // }
    this.toeicService
    .addCategory(this.createNewData())
    .subscribe((data) => {});
    this.reloadCurrentRoute();
  }
  public update(){
    this.toeicService
        .editCategory(this.id, this.createNewData())
        .subscribe((data) => {});
  }
  public save() {
    // if (this.id > 0) {
    //   this.toeicService
    //     .editCategory(this.id, this.createNewData())
    //     .subscribe((data) => {
    //       console.log('update');
    //     });
    // } else {

    // }
    this.toeicService.addCategory(this.createNewData()).subscribe((data) => {
      this.categoryForm.reset();
      console.log('re');
    });
    this.reloadCurrentRoute();
  }
  openEdit(targetModal, category: Category) {
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    },);

    this.editCategoryForm.patchValue( {

    });
  }


}
