import { Router } from '@angular/router';
import { Question } from 'src/app/common/models/questions.models';
import { ToeicService } from 'src/app/common/api-service/toeic.service';
import {AfterViewInit, Component, ViewChild, OnInit, Output} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { FormControl, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-d1-question',
  templateUrl: './d1-question.component.html',
  styleUrls: ['./d1-question.component.scss']
})
export class D1QuestionComponent implements AfterViewInit, OnInit {
  ELEMENT_DATA: Question[] = [];
  value='';
  displayedColumns : string[] = ['id','exams_id','question_index','question','anwser_a','anwser_b','anwser_c','anwser_d','correct','image_url','action'];
  dataSource = new MatTableDataSource<Question>(this.ELEMENT_DATA);

  constructor(private toeicService : ToeicService, private router :Router) { }
  ngOnInit(){
    this.getQuestiontable();
  }

  getQuestiontable(){
    let resp = this.toeicService.getAllQuestions();
    resp.subscribe(report=>this.dataSource.data=report as Question[])
  }

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter() {
    this.dataSource.filter = this.value.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onSearchClear() {
    this.value = "";
    this.applyFilter();
  }
  deleteQuestion(id: number) {
    this.toeicService.deleteQuestion(id).subscribe((data) => {
      console.log('delete', data);
      this.getQuestiontable();
    });
  }
  public questionForm = new FormGroup({
    exams_id: new FormControl(''),
    question_index: new FormControl(''),
    question: new FormControl(''),
    anwser_a: new FormControl(''),
    anwser_b: new FormControl(''),
    anwser_c: new FormControl(''),
    anwser_d: new FormControl(''),
    correct: new FormControl(''),
    image_url: new FormControl(''),
    audio: new FormControl(''),
  });
  public id = 0;
  private createNewData() {
    const newQuestions = {};
    for (const controlName in this.questionForm.controls) {
      if (controlName) {
        newQuestions[controlName] = this.questionForm.controls[
          controlName
        ].value;
      }
    }
    return newQuestions as Question;
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
  public saveAndGotoList() {
    // if (this.id > 0) {
    //   this.toeicService
    //     .editQuestion(this.id, this.createNewData())
    //     .subscribe((data) => {});
    // } else {

    // }
    this.toeicService
    .addQuestion(this.createNewData())
    .subscribe((data) => {});
    this.reloadCurrentRoute();
  }
  public update(){
    this.toeicService
        .editQuestion(this.id, this.createNewData())
        .subscribe((data) => {});
  }
  public save() {
    // if (this.id > 0) {
    //   this.toeicService
    //     .editQuestions(this.id, this.createNewData())
    //     .subscribe((data) => {
    //       console.log('update');
    //     });
    // } else {

    // }
    this.toeicService.addQuestion(this.createNewData()).subscribe((data) => {
      this.questionForm.reset();
      console.log('re');
    });
    this.reloadCurrentRoute();
  }
}
