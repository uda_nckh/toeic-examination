import { Question } from 'src/app/common/models/questions.models';
import { ToeicService } from 'src/app/common/api-service/toeic.service';
import {AfterViewInit, Component, ViewChild, OnInit, Output} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
@Component({
  selector: 'app-e1-list-user',
  templateUrl: './e1-list-user.component.html',
  styleUrls: ['./e1-list-user.component.scss']
})
export class E1ListUserComponent implements AfterViewInit, OnInit {
  ELEMENT_DATA: Question[] = [];
  value='';
  displayedColumns : string[] = ['id','exams_id','question','anwser_a','anwser_b','anwser_c','anwser_d','correct','image_url','action'];
  dataSource = new MatTableDataSource<Question>(this.ELEMENT_DATA);

  constructor(private toeicService : ToeicService) { }
  ngOnInit(){
    this.getQuestiontable();
  }

  getQuestiontable(){
    let resp = this.toeicService.getAllQuestions();
    resp.subscribe(report=>this.dataSource.data=report as Question[])
  }

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter() {
    this.dataSource.filter = this.value.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onSearchClear() {
    this.value = "";
    this.applyFilter();
  }
}
