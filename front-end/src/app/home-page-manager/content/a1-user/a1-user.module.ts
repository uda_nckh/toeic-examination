import { NgModule } from '@angular/core';
import { A1UserComponent } from './a1-user.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common'
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [A1UserComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '', component: A1UserComponent, children: [
        ],
      }
    ]),
  ]
})
export class A1UserModule { }
