import { ToeicService } from 'src/app/common/api-service/toeic.service';
import {AfterViewInit, Component, ViewChild, OnInit, Output} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { User } from 'src/app/common/models/users.models';


@Component({
  selector: "app-a1-user",
  templateUrl: "./a1-user.component.html",
  styleUrls: ["./a1-user.component.scss"],
})
export class A1UserComponent implements AfterViewInit, OnInit {
  ELEMENT_DATA: User[] = [];
  value='';
  displayedColumns : string[] = ['id','exams_id','question','anwser_a','anwser_b','anwser_c','anwser_d','correct','image_url','create_time','update_time','action'];
  dataSource = new MatTableDataSource<User>(this.ELEMENT_DATA);

  constructor(private toeicService : ToeicService) { }
  ngOnInit(){
    this.getUsertable();
  }

  getUsertable(){
    let resp = this.toeicService.getAllQuestions();
    resp.subscribe(report=>this.dataSource.data=report as User[])
  }

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter() {
    this.dataSource.filter = this.value.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onSearchClear() {
    this.value = "";
    this.applyFilter();
  }
}
