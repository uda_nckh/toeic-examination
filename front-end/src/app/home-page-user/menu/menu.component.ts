import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Router } from '@angular/router';
import { Subscription, Observable, Observer } from 'rxjs';
import { SendDataService } from 'src/app/common/api-service/send-data.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit, AfterViewInit, OnDestroy {
  subscription: Subscription[] = [];

  // language: string;

  // observable
  observable: Observable<any>;
  observer: Observer<any>;

  // data binding
  headerInfo = {
    Phone: '',
    Mail: '',
    mailHref: '',
    phoneHref: '',
  };

  dataMenu: any;
  amount: number = 0;
  idUser: string;
  infoUser: any;
  userValue: any = [];

  // binding logo
  logo: string = '';
  avatar: string = '';
  Promotion: any = {
    Name: '',
    Link: '',
  };
  isCheckPromotion: boolean = false;

  menuParents = [
    {
      id: '',
      IdParent: '',
      Link: '',
      Name: '',
      Position: '',
    },
  ];

  menuChild = [
    {
      id: '',
      IdParent: '',
      Link: '',
      Name: '',
      Position: '',
    },
  ];

  listCateRecipe: any;
  shopMenuCatgores: any[] = [];
  shopMenuMobile: any[] = [];
  isMobile: boolean = false;
  isShowMenuMobile: boolean = false;
  isDisplayNone: boolean = true;
  isClassShow: boolean = false;
  isClass1Show: boolean = false;
  isClass2Show: boolean = false;
  isClassShowAccountMobile: boolean = false;

  // candidateValue login

  /**
   * constructor
   * @param api
   */
  constructor(
    public api: ApiService,
    private sendDataService: SendDataService,
    private router: Router
  ) {
    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
    this.idUser = this.api.getStaffValue?.id;
    this.userValue = this.api.getStaffValue;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.onGetDataAll();



    if (this.userValue != null) {
      this.infoUser = this.userValue;

      // set avatar default user
      if (this.infoUser['Avatar'] == '' || this.infoUser['Avatar'] == null) {
        this.infoUser['Avatar'] = 'https://img.pngio.com/computer-icons-user-clip-art-transparent-user-icon-png-1742152-user-icons-png-920_641.png';
      } else {
        this.infoUser['Avatar'] = this.infoUser['Avatar'];
      }
    }


  }

  /**
   * ngAfterViewInit
   */
  ngAfterViewInit() { }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }
  /**
   * onGetDataAll
   */
  onGetDataAll() {
    const param = {
      IdUser: this.idUser,
    };
    this.subscription.push(
      this.api.excuteAllByWhat(param, '420', true).subscribe((result) => {
        let data_header = result.result_header.data;
        let data_recipe = result.result_recipe.data;
        let data_ads = result.result_ads.data;
        let data_cart = result.result_cart.data;

        //Header
        if (result.result_header.status == true && data_header.length > 0) {
          this.headerInfo = data_header[0];
          data_header[0].phoneHref = 'tel:' + this.headerInfo.Phone;
          data_header[0].mailHref = 'mailto:' + this.headerInfo.Mail;
        }

        //Recipe
        if (result.result_recipe.status == true && data_recipe.length > 0) {
          this.listCateRecipe = data_recipe;
        }

        //ADS
        if (result.result_ads.status == true && data_ads.length > 0) {
          this.Promotion = data_ads[0];
          this.isCheckPromotion = true;
        } else {
          this.Promotion = [];
          this.isCheckPromotion = false;
        }

        //Cart
        if (result.result_cart.status == true && data_cart.length > 0) {
          this.amount = Number(data_cart[0].count);
        } else {
          this.amount = 0;
        }

      })
    );
  }

  /**
  * isMobileDevice
  */
  isMobileDevice() {
    var userAgent = navigator.userAgent;
    if (/Android|webOS|iPhone|iPod|iPad|BlackBerry|IEMobile|Opera Mini/i.test(userAgent)) {
      // true for mobile device
      this.isMobile = true;
    } else {
      // false for not mobile device
      this.isMobile = false;
    }
  };

  /**
   * logOut
   */
  logOut() {
    this.api.logoutStaff();
  }
  goProfile(){
    this.router.navigate(['/a1-homepage/trang-chu']);
  }



  getDataMenuHead() {
    const param = {};
    this.subscription.push(
      this.api.excuteAllByWhat(param, '500', true).subscribe((result) => {
        let data = result.data;
        if (result.status == true && data.length > 0) {
          if (this.userValue == null) {
            data['6']['Link'] = '/login/dang-nhap';
          }

          (data || []).map((item) => {
            item['childs'] = [];
            (data || []).map((item1) => {
              if (+item1.IdParent === +item.id) {
                item['childs'].push(item1);

              }
            });
            return item;
          });

          this.dataMenu = data.filter((item) => +item.IdParent === 0);

          this.dataMenu.forEach(element => {
            element.nameCustom = element.Name;

          });
        }
      })
    );
  }


  /**
   * onBtnLoginSocialClick
   */
  onBtnLoginSocialClick() {
    const url = '/login/dang-nhap';
    localStorage.setItem('lastUrl', this.router.url);
    this.router.navigate([url.toLowerCase()]);
  }

  tofull(){
    document.getElementById("fulltest")?.scrollIntoView({ behavior: 'auto'})
  }
  tomini(){
    document.getElementById("minitest")?.scrollIntoView({ behavior: 'auto'})
  }
  tolisten(){
    document.getElementById("listening")?.scrollIntoView({ behavior: 'auto'})
  }
  toread(){
    document.getElementById("reading")?.scrollIntoView({ behavior: 'auto'})
  }

}
