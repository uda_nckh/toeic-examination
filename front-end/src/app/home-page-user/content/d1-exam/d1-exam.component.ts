
import { FormBuilder } from '@angular/forms';
import { ToeicService } from 'src/app/common/api-service/toeic.service';
import { CountdownComponent, CountdownEvent } from 'ngx-countdown';
import { Component, OnInit, OnDestroy, ViewChild, HostListener } from '@angular/core';
import { Event } from '@angular/router';
// import { ToeicService } from 'src/app/common/api-service/api.service';
// import { Router } from '@angular/router';
// import { FormControl, FormGroup } from '@angular/forms';
// import { ApiService } from './../../../common/api-service/toeic.service';


@Component({
  selector: 'app-d1-exam',
  templateUrl: './d1-exam.component.html',
  styleUrls: ['./d1-exam.component.scss'],
})
export class D1ExamComponent implements OnInit, OnDestroy {

  @HostListener('window:unload', [ '$event' ])
  unloadHandler(event: Event) {
    // If you want to do something when page gets reloaded or on leaving page
  }

  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHander(event: Event) {
    return false;
  }

  // @ViewChild('cd', { static: false }) counter: CountdownComponent;
  countdown: any;
  // time =7201000+new Date().getTime();
  condition: boolean = true
  isHidden: boolean = true;
  alert: boolean = false;
  isActive: boolean = false;
  isShow: boolean = false;
  ExamData: any[] = [];
  AnswerResult: any[] = [];
  part_read = 0;
  part_listen = 0;
  read = '';
  listen = '';
  total = '';
  scores_r = 0;
  scores_l = 0;
  total_scores = '';
  time: any
  x = setInterval(()=>{
    var now = new Date().getTime();
    this.time = now
  })

  answer1: any;
  answer2: any;
  answer3: any;
  answer4: any;
  answer5: any;
  answer6: any;
  answer7: any;
  answer8: any;
  answer9: any;
  answer10: any;
  answer11: any;
  answer12: any;
  answer13: any;
  answer14: any;
  answer15: any;
  answer16: any;
  answer17: any;
  answer18: any;
  answer19: any;
  answer20: any;
  answer21: any;
  answer22: any;
  answer23: any;
  answer24: any;
  answer25: any;
  answer26: any;
  answer27: any;
  answer28: any;
  answer29: any;
  answer30: any;
  answer31: any;
  answer32: any;
  answer33: any;
  answer34: any;
  answer35: any;
  answer36: any;
  answer37: any;
  answer38: any;
  answer39: any;
  answer40: any;
  answer41: any;
  answer42: any;
  answer43: any;
  answer44: any;
  answer45: any;
  answer46: any;
  answer47: any;
  answer48: any;
  answer49: any;
  answer50: any;
  answer51: any;
  answer52: any;
  answer53: any;
  answer54: any;
  answer55: any;
  answer56: any;
  answer57: any;
  answer58: any;
  answer59: any;
  answer60: any;
  answer61: any;
  answer62: any;
  answer63: any;
  answer64: any;
  answer65: any;
  answer66: any;
  answer67: any;
  answer68: any;
  answer69: any;
  answer70: any;
  answer71: any;
  answer72: any;
  answer73: any;
  answer74: any;
  answer75: any;
  answer76: any;
  answer77: any;
  answer78: any;
  answer79: any;
  answer80: any;
  answer81: any;
  answer82: any;
  answer83: any;
  answer84: any;
  answer85: any;
  answer86: any;
  answer87: any;
  answer88: any;
  answer89: any;
  answer90: any;
  answer91: any;
  answer92: any;
  answer93: any;
  answer94: any;
  answer95: any;
  answer96: any;
  answer97: any;
  answer98: any;
  answer99: any;
  answer100: any;
  answer101: any;
  answer102: any;
  answer103: any;
  answer104: any;
  answer105: any;
  answer106: any;
  answer107: any;
  answer108: any;
  answer109: any;
  answer110: any;
  answer111: any;
  answer112: any;
  answer113: any;
  answer114: any;
  answer115: any;
  answer116: any;
  answer117: any;
  answer118: any;
  answer119: any;
  answer120: any;
  answer121: any;
  answer122: any;
  answer123: any;
  answer124: any;
  answer125: any;
  answer126: any;
  answer127: any;
  answer128: any;
  answer129: any;
  answer130: any;
  answer131: any;
  answer132: any;
  answer133: any;
  answer134: any;
  answer135: any;
  answer136: any;
  answer137: any;
  answer138: any;
  answer139: any;
  answer140: any;
  answer141: any;
  answer142: any;
  answer143: any;
  answer144: any;
  answer145: any;
  answer146: any;
  answer147: any;
  answer148: any;
  answer149: any;
  answer150: any;
  answer151: any;
  answer152: any;
  answer153: any;
  answer154: any;
  answer155: any;
  answer156: any;
  answer157: any;
  answer158: any;
  answer159: any;
  answer160: any;
  answer161: any;
  answer162: any;
  answer163: any;
  answer164: any;
  answer165: any;
  answer166: any;
  answer167: any;
  answer168: any;
  answer169: any;
  answer170: any;
  answer171: any;
  answer172: any;
  answer173: any;
  answer174: any;
  answer175: any;
  answer176: any;
  answer177: any;
  answer178: any;
  answer179: any;
  answer180: any;
  answer181: any;
  answer182: any;
  answer183: any;
  answer184: any;
  answer185: any;
  answer186: any;
  answer187: any;
  answer188: any;
  answer189: any;
  answer190: any;
  answer191: any;
  answer192: any;
  answer193: any;
  answer194: any;
  answer195: any;
  answer196: any;
  answer197: any;
  answer198: any;
  answer199: any;
  answer200: any;

  constructor(private toeicService: ToeicService, private fb: FormBuilder,) {}

  ngOnDestroy(): void {
    // throw new Error('Method not implemented.');
  }
  ngOnInit(): void {
    this.toeicService.getQuestionByExamId().subscribe((data) => {
      console.log(data);
      this.ExamData = data;
      // for (let i = 0; i < this.ExamData.length; i++) {
      //   console.log("qi"+i,data[i].question_index)
      // }
    });
  }

  onTimerFinished(e: CountdownEvent) {
    if (e.action == 'done') {
      var element = document.getElementById("exampleModal");
      element.classList.add("show");
      element.style.display = "block"
      this.alert = true;
    }
  }
  close(){
    var element = document.getElementById("exampleModal");
      element.classList.remove("show");
      element.style.display = "none"
  }
  submit(){
    this.close()
    this.isHidden = false;
    this.condition = false;
    // this.AnswerResult.splice(0,0,this.answer)
    this.AnswerResult.push(this.answer1)
    this.AnswerResult.push(this.answer2)
    this.AnswerResult.push(this.answer3)
    this.AnswerResult.push(this.answer4)
    this.AnswerResult.push(this.answer5)
    this.AnswerResult.push(this.answer6)
    this.AnswerResult.push(this.answer7)
    this.AnswerResult.push(this.answer8)
    this.AnswerResult.push(this.answer9)
    this.AnswerResult.push(this.answer10)
    this.AnswerResult.push(this.answer11)
    this.AnswerResult.push(this.answer12)
    this.AnswerResult.push(this.answer13)
    this.AnswerResult.push(this.answer14)
    this.AnswerResult.push(this.answer15)
    this.AnswerResult.push(this.answer16)
    this.AnswerResult.push(this.answer17)
    this.AnswerResult.push(this.answer18)
    this.AnswerResult.push(this.answer19)
    this.AnswerResult.push(this.answer20)
    this.AnswerResult.push(this.answer21)
    this.AnswerResult.push(this.answer22)
    this.AnswerResult.push(this.answer23)
    this.AnswerResult.push(this.answer24)
    this.AnswerResult.push(this.answer25)
    this.AnswerResult.push(this.answer26)
    this.AnswerResult.push(this.answer27)
    this.AnswerResult.push(this.answer28)
    this.AnswerResult.push(this.answer29)
    this.AnswerResult.push(this.answer30)
    this.AnswerResult.push(this.answer31)
    this.AnswerResult.push(this.answer32)
    this.AnswerResult.push(this.answer33)
    this.AnswerResult.push(this.answer34)
    this.AnswerResult.push(this.answer35)
    this.AnswerResult.push(this.answer36)
    this.AnswerResult.push(this.answer37)
    this.AnswerResult.push(this.answer38)
    this.AnswerResult.push(this.answer39)
    this.AnswerResult.push(this.answer40)
    this.AnswerResult.push(this.answer41)
    this.AnswerResult.push(this.answer42)
    this.AnswerResult.push(this.answer43)
    this.AnswerResult.push(this.answer44)
    this.AnswerResult.push(this.answer45)
    this.AnswerResult.push(this.answer46)
    this.AnswerResult.push(this.answer47)
    this.AnswerResult.push(this.answer48)
    this.AnswerResult.push(this.answer49)
    this.AnswerResult.push(this.answer50)
    this.AnswerResult.push(this.answer51)
    this.AnswerResult.push(this.answer52)
    this.AnswerResult.push(this.answer53)
    this.AnswerResult.push(this.answer54)
    this.AnswerResult.push(this.answer55)
    this.AnswerResult.push(this.answer56)
    this.AnswerResult.push(this.answer57)
    this.AnswerResult.push(this.answer58)
    this.AnswerResult.push(this.answer59)
    this.AnswerResult.push(this.answer60)
    this.AnswerResult.push(this.answer61)
    this.AnswerResult.push(this.answer62)
    this.AnswerResult.push(this.answer63)
    this.AnswerResult.push(this.answer64)
    this.AnswerResult.push(this.answer65)
    this.AnswerResult.push(this.answer66)
    this.AnswerResult.push(this.answer67)
    this.AnswerResult.push(this.answer68)
    this.AnswerResult.push(this.answer69)
    this.AnswerResult.push(this.answer70)
    this.AnswerResult.push(this.answer71)
    this.AnswerResult.push(this.answer72)
    this.AnswerResult.push(this.answer73)
    this.AnswerResult.push(this.answer74)
    this.AnswerResult.push(this.answer75)
    this.AnswerResult.push(this.answer76)
    this.AnswerResult.push(this.answer77)
    this.AnswerResult.push(this.answer78)
    this.AnswerResult.push(this.answer79)
    this.AnswerResult.push(this.answer80)
    this.AnswerResult.push(this.answer81)
    this.AnswerResult.push(this.answer82)
    this.AnswerResult.push(this.answer83)
    this.AnswerResult.push(this.answer84)
    this.AnswerResult.push(this.answer85)
    this.AnswerResult.push(this.answer86)
    this.AnswerResult.push(this.answer87)
    this.AnswerResult.push(this.answer88)
    this.AnswerResult.push(this.answer89)
    this.AnswerResult.push(this.answer90)
    this.AnswerResult.push(this.answer91)
    this.AnswerResult.push(this.answer92)
    this.AnswerResult.push(this.answer93)
    this.AnswerResult.push(this.answer94)
    this.AnswerResult.push(this.answer95)
    this.AnswerResult.push(this.answer96)
    this.AnswerResult.push(this.answer97)
    this.AnswerResult.push(this.answer98)
    this.AnswerResult.push(this.answer99)
    this.AnswerResult.push(this.answer100)
    this.AnswerResult.push(this.answer101)
    this.AnswerResult.push(this.answer102)
    this.AnswerResult.push(this.answer103)
    this.AnswerResult.push(this.answer104)
    this.AnswerResult.push(this.answer105)
    this.AnswerResult.push(this.answer106)
    this.AnswerResult.push(this.answer107)
    this.AnswerResult.push(this.answer108)
    this.AnswerResult.push(this.answer109)
    this.AnswerResult.push(this.answer110)
    this.AnswerResult.push(this.answer111)
    this.AnswerResult.push(this.answer112)
    this.AnswerResult.push(this.answer113)
    this.AnswerResult.push(this.answer114)
    this.AnswerResult.push(this.answer115)
    this.AnswerResult.push(this.answer116)
    this.AnswerResult.push(this.answer117)
    this.AnswerResult.push(this.answer118)
    this.AnswerResult.push(this.answer119)
    this.AnswerResult.push(this.answer120)
    this.AnswerResult.push(this.answer121)
    this.AnswerResult.push(this.answer122)
    this.AnswerResult.push(this.answer123)
    this.AnswerResult.push(this.answer124)
    this.AnswerResult.push(this.answer125)
    this.AnswerResult.push(this.answer126)
    this.AnswerResult.push(this.answer127)
    this.AnswerResult.push(this.answer128)
    this.AnswerResult.push(this.answer129)
    this.AnswerResult.push(this.answer130)
    this.AnswerResult.push(this.answer131)
    this.AnswerResult.push(this.answer132)
    this.AnswerResult.push(this.answer133)
    this.AnswerResult.push(this.answer134)
    this.AnswerResult.push(this.answer135)
    this.AnswerResult.push(this.answer136)
    this.AnswerResult.push(this.answer137)
    this.AnswerResult.push(this.answer138)
    this.AnswerResult.push(this.answer139)
    this.AnswerResult.push(this.answer140)
    this.AnswerResult.push(this.answer141)
    this.AnswerResult.push(this.answer142)
    this.AnswerResult.push(this.answer143)
    this.AnswerResult.push(this.answer144)
    this.AnswerResult.push(this.answer145)
    this.AnswerResult.push(this.answer146)
    this.AnswerResult.push(this.answer147)
    this.AnswerResult.push(this.answer148)
    this.AnswerResult.push(this.answer149)
    this.AnswerResult.push(this.answer150)
    this.AnswerResult.push(this.answer151)
    this.AnswerResult.push(this.answer152)
    this.AnswerResult.push(this.answer153)
    this.AnswerResult.push(this.answer154)
    this.AnswerResult.push(this.answer155)
    this.AnswerResult.push(this.answer156)
    this.AnswerResult.push(this.answer157)
    this.AnswerResult.push(this.answer158)
    this.AnswerResult.push(this.answer159)
    this.AnswerResult.push(this.answer160)
    this.AnswerResult.push(this.answer161)
    this.AnswerResult.push(this.answer162)
    this.AnswerResult.push(this.answer163)
    this.AnswerResult.push(this.answer164)
    this.AnswerResult.push(this.answer165)
    this.AnswerResult.push(this.answer166)
    this.AnswerResult.push(this.answer167)
    this.AnswerResult.push(this.answer168)
    this.AnswerResult.push(this.answer169)
    this.AnswerResult.push(this.answer170)
    this.AnswerResult.push(this.answer171)
    this.AnswerResult.push(this.answer172)
    this.AnswerResult.push(this.answer173)
    this.AnswerResult.push(this.answer174)
    this.AnswerResult.push(this.answer175)
    this.AnswerResult.push(this.answer176)
    this.AnswerResult.push(this.answer177)
    this.AnswerResult.push(this.answer178)
    this.AnswerResult.push(this.answer179)
    this.AnswerResult.push(this.answer180)
    this.AnswerResult.push(this.answer181)
    this.AnswerResult.push(this.answer182)
    this.AnswerResult.push(this.answer183)
    this.AnswerResult.push(this.answer184)
    this.AnswerResult.push(this.answer185)
    this.AnswerResult.push(this.answer186)
    this.AnswerResult.push(this.answer187)
    this.AnswerResult.push(this.answer188)
    this.AnswerResult.push(this.answer189)
    this.AnswerResult.push(this.answer190)
    this.AnswerResult.push(this.answer191)
    this.AnswerResult.push(this.answer192)
    this.AnswerResult.push(this.answer193)
    this.AnswerResult.push(this.answer194)
    this.AnswerResult.push(this.answer195)
    this.AnswerResult.push(this.answer196)
    this.AnswerResult.push(this.answer197)
    this.AnswerResult.push(this.answer198)
    this.AnswerResult.push(this.answer199)
    this.AnswerResult.push(this.answer200)

    console.log("rs",this.AnswerResult)

    for (let i = 0; i < 100; i++) {
      if (this.AnswerResult[i]===this.ExamData[i].correct) {
        this.part_listen++
      }else{
        document.getElementById(""+(i+1)).classList.add("false");
      }
      // console.log("rs",this.AnswerResult[i])
      // console.log("data",this.ExamData[i].correct)
    }



    for (let i = 100; i < this.AnswerResult.length; i++) {
      if (this.AnswerResult[i]===this.ExamData[i].correct) {
        this.part_read++
      }else{
        document.getElementById(""+(i+1)).classList.add("false");
      }
      // console.log("rs",this.AnswerResult[i])
      // console.log("data",this.ExamData[i].correct)
    }
    this.read = this.part_read + "/100";
    this.listen = this.part_listen + "/100";
    this.total = (this.part_read + this.part_listen) + "/200";

    // console.log('lt',this.part_listen)
    // console.log('r',this.part_read)

    //scores listening
    if (this.part_listen==0) {
      this.scores_l=5
    }
    if(this.part_listen==1) {
      this.scores_l=15
    }
    if(this.part_listen==2) {
      this.scores_l=20
    }
    if(this.part_listen==3) {
      this.scores_l=25
    }
    if(this.part_listen==4) {
      this.scores_l=30
    }
    if(this.part_listen==5) {
      this.scores_l=35
    }
    if(this.part_listen==6) {
      this.scores_l=40
    }
    if(this.part_listen==7) {
      this.scores_l=45
    }
    if(this.part_listen==8) {
      this.scores_l=50
    }
    if(this.part_listen==9) {
      this.scores_l=55
    }
    if(this.part_listen==10) {
      this.scores_l=60
    }
    if(this.part_listen==11) {
      this.scores_l=65
    }
    if(this.part_listen==12) {
      this.scores_l=70
    }
    if(this.part_listen==13) {
      this.scores_l=75
    }
    if(this.part_listen==14) {
      this.scores_l=80
    }
    if(this.part_listen==15) {
      this.scores_l=85
    }
    if(this.part_listen==16) {
      this.scores_l=90
    }
    if(this.part_listen==17) {
      this.scores_l=95
    }
    if(this.part_listen==18) {
      this.scores_l=100
    }
    if(this.part_listen==19) {
      this.scores_l=105
    }
    if(this.part_listen==20) {
      this.scores_l=110
    }
    if(this.part_listen==21) {
      this.scores_l=115
    }
    if(this.part_listen==22) {
      this.scores_l=120
    }
    if(this.part_listen==23) {
      this.scores_l=125
    }
    if(this.part_listen==24) {
      this.scores_l=130
    }
    if(this.part_listen==25) {
      this.scores_l=135
    }
    if(this.part_listen==26) {
      this.scores_l=140
    }
    if(this.part_listen==27) {
      this.scores_l=145
    }
    if(this.part_listen==28) {
      this.scores_l=150
    }
    if(this.part_listen==29) {
      this.scores_l=155
    }
    if(this.part_listen==30) {
      this.scores_l=160
    }
    if(this.part_listen==31) {
      this.scores_l=165
    }
    if(this.part_listen==32) {
      this.scores_l=170
    }
    if(this.part_listen==33) {
      this.scores_l=175
    }
    if(this.part_listen==34) {
      this.scores_l=180
    }
    if(this.part_listen==35) {
      this.scores_l=185
    }
    if(this.part_listen==36) {
      this.scores_l=190
    }
    if(this.part_listen==37) {
      this.scores_l=195
    }
    if(this.part_listen==38) {
      this.scores_l=200
    }
    if(this.part_listen==39) {
      this.scores_l=205
    }
    if(this.part_listen==40) {
      this.scores_l=210
    }
    if(this.part_listen==41) {
      this.scores_l=215
    }
    if(this.part_listen==42) {
      this.scores_l=220
    }
    if(this.part_listen==43) {
      this.scores_l=225
    }
    if(this.part_listen==44) {
      this.scores_l=230
    }
    if(this.part_listen==45) {
      this.scores_l=235
    }
    if(this.part_listen==46) {
      this.scores_l=240
    }
    if(this.part_listen==47) {
      this.scores_l=245
    }
    if(this.part_listen==48) {
      this.scores_l=250
    }
    if(this.part_listen==49) {
      this.scores_l=255
    }
    if(this.part_listen==50) {
      this.scores_l=260
    }
    if(this.part_listen==51) {
      this.scores_l=265
    }
    if(this.part_listen==52) {
      this.scores_l=270
    }
    if(this.part_listen==53) {
      this.scores_l=275
    }
    if(this.part_listen==54) {
      this.scores_l=280
    }
    if(this.part_listen==55) {
      this.scores_l=285
    }
    if(this.part_listen==56) {
      this.scores_l=290
    }
    if(this.part_listen==57) {
      this.scores_l=295
    }
    if(this.part_listen==58) {
      this.scores_l=300
    }
    if(this.part_listen==59) {
      this.scores_l=305
    }
    if(this.part_listen==60) {
      this.scores_l=310
    }
    if(this.part_listen==61) {
      this.scores_l=315
    }
    if(this.part_listen==62) {
      this.scores_l=320
    }
    if(this.part_listen==63) {
      this.scores_l=325
    }
    if(this.part_listen==64) {
      this.scores_l=330
    }
    if(this.part_listen==65) {
      this.scores_l=335
    }
    if(this.part_listen==66) {
      this.scores_l=340
    }
    if(this.part_listen==67) {
      this.scores_l=345
    }
    if(this.part_listen==68) {
      this.scores_l=350
    }
    if(this.part_listen==69) {
      this.scores_l=355
    }
    if(this.part_listen==70) {
      this.scores_l=360
    }
    if(this.part_listen==71) {
      this.scores_l=365
    }
    if(this.part_listen==72) {
      this.scores_l=370
    }
    if(this.part_listen==73) {
      this.scores_l=375
    }
    if(this.part_listen==74) {
      this.scores_l=380
    }
    if(this.part_listen==75) {
      this.scores_l=385
    }
    if(this.part_listen==76) {
      this.scores_l=395
    }
    if(this.part_listen==77) {
      this.scores_l=400
    }
    if(this.part_listen==78) {
      this.scores_l=405
    }
    if(this.part_listen==79) {
      this.scores_l=410
    }
    if(this.part_listen==80) {
      this.scores_l=415
    }
    if(this.part_listen==81) {
      this.scores_l=420
    }
    if(this.part_listen==82) {
      this.scores_l=425
    }
    if(this.part_listen==83) {
      this.scores_l=430
    }
    if(this.part_listen==84) {
      this.scores_l=435
    }
    if(this.part_listen==85) {
      this.scores_l=440
    }
    if(this.part_listen==86) {
      this.scores_l=445
    }
    if(this.part_listen==87) {
      this.scores_l=450
    }
    if(this.part_listen==88) {
      this.scores_l=455
    }
    if(this.part_listen==89) {
      this.scores_l=460
    }
    if(this.part_listen==90) {
      this.scores_l=465
    }
    if(this.part_listen==91) {
      this.scores_l=470
    }
    if(this.part_listen==92) {
      this.scores_l=475
    }
    if(this.part_listen==93) {
      this.scores_l=480
    }
    if(this.part_listen==94) {
      this.scores_l=485
    }
    if(this.part_listen==95) {
      this.scores_l=490
    }
    if(this.part_listen==96) {
      this.scores_l=495
    }
    if(this.part_listen==97) {
      this.scores_l=495
    }
    if(this.part_listen==98) {
      this.scores_l=495
    }
    if(this.part_listen==99) {
      this.scores_l=495
    }
    if(this.part_listen==100) {
      this.scores_l=495
    }

    //scores reading
    if (this.part_read>=0 && this.part_read<=2) {
      this.scores_r=5
    }
    if(this.part_read==3) {
      this.scores_r=10
    }
    if(this.part_read==4) {
      this.scores_r=15
    }
    if(this.part_read==5) {
      this.scores_r=20
    }
    if(this.part_read==6) {
      this.scores_r=25
    }
    if(this.part_read==7) {
      this.scores_r=30
    }
    if(this.part_read==8) {
      this.scores_r=35
    }
    if(this.part_read==9) {
      this.scores_r=40
    }
    if(this.part_read==10) {
      this.scores_r=45
    }
    if(this.part_read==11) {
      this.scores_r=50
    }
    if(this.part_read==12) {
      this.scores_r=55
    }
    if(this.part_read==13) {
      this.scores_r=60
    }
    if(this.part_read==14) {
      this.scores_r=65
    }
    if(this.part_read==15) {
      this.scores_r=70
    }
    if(this.part_read==16) {
      this.scores_r=75
    }
    if(this.part_read==17) {
      this.scores_r=80
    }
    if(this.part_read==18) {
      this.scores_r=85
    }
    if(this.part_read==19) {
      this.scores_r=90
    }
    if(this.part_read==20) {
      this.scores_r=95
    }
    if(this.part_read==21) {
      this.scores_r=100
    }
    if(this.part_read==22) {
      this.scores_r=105
    }
    if(this.part_read==23) {
      this.scores_r=110
    }
    if(this.part_read==24) {
      this.scores_r=115
    }
    if(this.part_read==25) {
      this.scores_r=120
    }
    if(this.part_read==26) {
      this.scores_r=125
    }
    if(this.part_read==27) {
      this.scores_r=130
    }
    if(this.part_read==28) {
      this.scores_r=135
    }
    if(this.part_read==29) {
      this.scores_r=140
    }
    if(this.part_read==30) {
      this.scores_r=145
    }
    if(this.part_read==31) {
      this.scores_r=150
    }
    if(this.part_read==32) {
      this.scores_r=155
    }
    if(this.part_read==33) {
      this.scores_r=160
    }
    if(this.part_read==34) {
      this.scores_r=165
    }
    if(this.part_read==35) {
      this.scores_r=170
    }
    if(this.part_read==36) {
      this.scores_r=175
    }
    if(this.part_read==37) {
      this.scores_r=180
    }
    if(this.part_read==38) {
      this.scores_r=185
    }
    if(this.part_read==39) {
      this.scores_r=190
    }
    if(this.part_read==40) {
      this.scores_r=195
    }
    if(this.part_read==41) {
      this.scores_r=200
    }
    if(this.part_read==42) {
      this.scores_r=205
    }
    if(this.part_read==43) {
      this.scores_r=210
    }
    if(this.part_read==44) {
      this.scores_r=215
    }
    if(this.part_read==45) {
      this.scores_r=220
    }
    if(this.part_read==46) {
      this.scores_r=225
    }
    if(this.part_read==47) {
      this.scores_r=230
    }
    if(this.part_read==48) {
      this.scores_r=235
    }
    if(this.part_read==49) {
      this.scores_r=240
    }
    if(this.part_read==50) {
      this.scores_r=245
    }
    if(this.part_read==51) {
      this.scores_r=250
    }
    if(this.part_read==52) {
      this.scores_r=255
    }
    if(this.part_read==53) {
      this.scores_r=260
    }
    if(this.part_read==54) {
      this.scores_r=265
    }
    if(this.part_read==55) {
      this.scores_r=270
    }
    if(this.part_read==56) {
      this.scores_r=275
    }
    if(this.part_read==57) {
      this.scores_r=280
    }
    if(this.part_read==58) {
      this.scores_r=285
    }
    if(this.part_read==59) {
      this.scores_r=290
    }
    if(this.part_read==60) {
      this.scores_r=295
    }
    if(this.part_read==61) {
      this.scores_r=300
    }
    if(this.part_read==62) {
      this.scores_r=305
    }
    if(this.part_read==63) {
      this.scores_r=310
    }
    if(this.part_read==64) {
      this.scores_r=315
    }
    if(this.part_read==65) {
      this.scores_r=320
    }
    if(this.part_read==66) {
      this.scores_r=325
    }
    if(this.part_read==67) {
      this.scores_r=330
    }
    if(this.part_read==68) {
      this.scores_r=335
    }
    if(this.part_read==69) {
      this.scores_r=340
    }
    if(this.part_read==70) {
      this.scores_r=345
    }
    if(this.part_read==71) {
      this.scores_r=350
    }
    if(this.part_read==72) {
      this.scores_r=355
    }
    if(this.part_read==73) {
      this.scores_r=360
    }
    if(this.part_read==74) {
      this.scores_r=365
    }
    if(this.part_read==75) {
      this.scores_r=370
    }
    if(this.part_read==76) {
      this.scores_r=375
    }
    if(this.part_read==77) {
      this.scores_r=380
    }
    if(this.part_read==78) {
      this.scores_r=385
    }
    if(this.part_read==79) {
      this.scores_r=390
    }
    if(this.part_read==80) {
      this.scores_r=395
    }
    if(this.part_read==81) {
      this.scores_r=400
    }
    if(this.part_read==82) {
      this.scores_r=405
    }
    if(this.part_read==83) {
      this.scores_r=410
    }
    if(this.part_read==84) {
      this.scores_r=415
    }
    if(this.part_read==85) {
      this.scores_r=420
    }
    if(this.part_read==86) {
      this.scores_r=425
    }
    if(this.part_read==87) {
      this.scores_r=430
    }
    if(this.part_read==88) {
      this.scores_r=435
    }
    if(this.part_read==89) {
      this.scores_r=440
    }
    if(this.part_read==90) {
      this.scores_r=445
    }
    if(this.part_read==91) {
      this.scores_r=450
    }
    if(this.part_read==92) {
      this.scores_r=455
    }
    if(this.part_read==93) {
      this.scores_r=460
    }
    if(this.part_read==94) {
      this.scores_r=465
    }
    if(this.part_read==95) {
      this.scores_r=470
    }
    if(this.part_read==96) {
      this.scores_r=475
    }
    if(this.part_read==97) {
      this.scores_r=485
    }
    if(this.part_read==98) {
      this.scores_r=485
    }
    if(this.part_read==99) {
      this.scores_r=490
    }
    if(this.part_read==100) {
      this.scores_r=495
    }
    this.total_scores = this.scores_l + this.scores_r + " Points";
  }
  focuspart2() {
    this.isActive = true;
    this.isShow = true;
  }

  toqs(id: number) {
    if (1<=id && id<=6) {
      document.getElementById("p1").classList.add("active", "show");
      document.getElementById("part1").classList.add("in", "active", "show");
      document.getElementById("p2").classList.remove("active", "show");
      document.getElementById("part2").classList.remove("in", "active", "show");
      document.getElementById("p3").classList.remove("active", "show");
      document.getElementById("part3").classList.remove("in", "active", "show");
      document.getElementById("p4").classList.remove("active", "show");
      document.getElementById("part4").classList.remove("in", "active", "show");
      document.getElementById("p5").classList.remove("active", "show");
      document.getElementById("part5").classList.remove("in", "active", "show");
      document.getElementById("p6").classList.remove("active", "show");
      document.getElementById("part6").classList.remove("in", "active", "show");
      document.getElementById("p7").classList.remove("active", "show");
      document.getElementById("part7").classList.remove("in", "active", "show");
      document.getElementById('to' + id).scrollIntoView({ behavior: 'auto' });
    }
    if (7<=id && id<=31) {
      document.getElementById("p2").classList.add("active", "show");
      document.getElementById("part2").classList.add("in", "active", "show");
      document.getElementById("p1").classList.remove("active", "show");
      document.getElementById("part1").classList.remove("in", "active", "show");
      document.getElementById("p3").classList.remove("active", "show");
      document.getElementById("part3").classList.remove("in", "active", "show");
      document.getElementById("p4").classList.remove("active", "show");
      document.getElementById("part4").classList.remove("in", "active", "show");
      document.getElementById("p5").classList.remove("active", "show");
      document.getElementById("part5").classList.remove("in", "active", "show");
      document.getElementById("p6").classList.remove("active", "show");
      document.getElementById("part6").classList.remove("in", "active", "show");
      document.getElementById("p7").classList.remove("active", "show");
      document.getElementById("part7").classList.remove("in", "active", "show");
      document.getElementById('to' + id).scrollIntoView({ behavior: 'auto' });
    }
    if (32<=id && id<=70) {
      document.getElementById("p3").classList.add("active", "show");
      document.getElementById("part3").classList.add("in", "active", "show");
      document.getElementById("p1").classList.remove("active", "show");
      document.getElementById("part1").classList.remove("in", "active", "show");
      document.getElementById("p2").classList.remove("active", "show");
      document.getElementById("part2").classList.remove("in", "active", "show");
      document.getElementById("p4").classList.remove("active", "show");
      document.getElementById("part4").classList.remove("in", "active", "show");
      document.getElementById("p5").classList.remove("active", "show");
      document.getElementById("part5").classList.remove("in", "active", "show");
      document.getElementById("p6").classList.remove("active", "show");
      document.getElementById("part6").classList.remove("in", "active", "show");
      document.getElementById("p7").classList.remove("active", "show");
      document.getElementById("part7").classList.remove("in", "active", "show");
      document.getElementById('to' + id).scrollIntoView({ behavior: 'auto' });
    }
    if (71<=id && id<=100) {
      document.getElementById("p4").classList.add("active", "show");
      document.getElementById("part4").classList.add("in", "active", "show");
      document.getElementById("p1").classList.remove("active", "show");
      document.getElementById("part1").classList.remove("in", "active", "show");
      document.getElementById("p2").classList.remove("active", "show");
      document.getElementById("part2").classList.remove("in", "active", "show");
      document.getElementById("p3").classList.remove("active", "show");
      document.getElementById("part3").classList.remove("in", "active", "show");
      document.getElementById("p5").classList.remove("active", "show");
      document.getElementById("part5").classList.remove("in", "active", "show");
      document.getElementById("p6").classList.remove("active", "show");
      document.getElementById("part6").classList.remove("in", "active", "show");
      document.getElementById("p7").classList.remove("active", "show");
      document.getElementById("part7").classList.remove("in", "active", "show");
      document.getElementById('to' + id).scrollIntoView({ behavior: 'auto' });
    }
    if (101<=id && id<=130) {
      document.getElementById("p5").classList.add("active", "show");
      document.getElementById("part5").classList.add("in", "active", "show");
      document.getElementById("p1").classList.remove("active", "show");
      document.getElementById("part1").classList.remove("in", "active", "show");
      document.getElementById("p2").classList.remove("active", "show");
      document.getElementById("part2").classList.remove("in", "active", "show");
      document.getElementById("p3").classList.remove("active", "show");
      document.getElementById("part3").classList.remove("in", "active", "show");
      document.getElementById("p4").classList.remove("active", "show");
      document.getElementById("part4").classList.remove("in", "active", "show");
      document.getElementById("p6").classList.remove("active", "show");
      document.getElementById("part6").classList.remove("in", "active", "show");
      document.getElementById("p7").classList.remove("active", "show");
      document.getElementById("part7").classList.remove("in", "active", "show");
      document.getElementById('to' + id).scrollIntoView({ behavior: 'auto' });
    }
    if (131<=id && id<=146) {
      document.getElementById("p6").classList.add("active", "show");
      document.getElementById("part6").classList.add("in", "active", "show");
      document.getElementById("p1").classList.remove("active", "show");
      document.getElementById("part1").classList.remove("in", "active", "show");
      document.getElementById("p2").classList.remove("active", "show");
      document.getElementById("part2").classList.remove("in", "active", "show");
      document.getElementById("p3").classList.remove("active", "show");
      document.getElementById("part3").classList.remove("in", "active", "show");
      document.getElementById("p4").classList.remove("active", "show");
      document.getElementById("part4").classList.remove("in", "active", "show");
      document.getElementById("p5").classList.remove("active", "show");
      document.getElementById("part5").classList.remove("in", "active", "show");
      document.getElementById("p7").classList.remove("active", "show");
      document.getElementById("part7").classList.remove("in", "active", "show");
      document.getElementById('to' + id).scrollIntoView({ behavior: 'auto' });
    }
    if (147<=id && id<=200) {
      document.getElementById("p7").classList.add("active", "show");
      document.getElementById("part7").classList.add("in", "active", "show");
      document.getElementById("p1").classList.remove("active", "show");
      document.getElementById("part1").classList.remove("in", "active", "show");
      document.getElementById("p2").classList.remove("active", "show");
      document.getElementById("part2").classList.remove("in", "active", "show");
      document.getElementById("p3").classList.remove("active", "show");
      document.getElementById("part3").classList.remove("in", "active", "show");
      document.getElementById("p4").classList.remove("active", "show");
      document.getElementById("part4").classList.remove("in", "active", "show");
      document.getElementById("p5").classList.remove("active", "show");
      document.getElementById("part5").classList.remove("in", "active", "show");
      document.getElementById("p6").classList.remove("active", "show");
      document.getElementById("part6").classList.remove("in", "active", "show");
      document.getElementById('to' + id).scrollIntoView({ behavior: 'auto' });
    }
  }
  toheader(){
    document.getElementById("header").scrollIntoView({ behavior: 'auto'});
  }
}


