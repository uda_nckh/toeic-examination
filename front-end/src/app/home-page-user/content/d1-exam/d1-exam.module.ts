import { TransferHttpCacheModule } from '@nguniversal/common';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { D1ExamComponent } from './d1-exam.component';
import { RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
  declarations: [D1ExamComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    LazyLoadImageModule,
    FormsModule,
    ReactiveFormsModule,
    CountdownModule,
    RouterModule.forChild([
      {
        path: '',
        component: D1ExamComponent,
        children: []
      }
    ]),
  ]
})
export class D1ExamModule { }
