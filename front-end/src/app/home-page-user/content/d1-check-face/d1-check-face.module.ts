import { TransferHttpCacheModule } from '@nguniversal/common';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { D1CheckFaceComponent } from './d1-check-face.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [D1CheckFaceComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: D1CheckFaceComponent,
        children: []
      },

    ]),
  ]
})
export class D1CkeckFaceModule { }
