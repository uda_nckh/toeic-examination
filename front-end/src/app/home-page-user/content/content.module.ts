import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';

@NgModule({
  declarations: [ContentComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentComponent,
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./a1-homepage/a1-homepage.module').then(
                (m) => m.A1HomePageModule
              ),
          },
          {
            path: 'a1-homepage/:title',
            loadChildren: () =>
              import('./a1-homepage/a1-homepage.module').then(
                (m) => m.A1HomePageModule
              ),
          },
          {
            path: 'd1-check-face/:title',
            loadChildren: () =>
              import('./d1-check-face/d1-check-face.module').then(
                (m) => m.D1CkeckFaceModule
              ),
          },
          {
            path: 'd1-exam/:title',
            loadChildren: () =>
              import('./d1-exam/d1-exam.module').then((m) => m.D1ExamModule),
          },
          {
            path: 'b1-list-recipe/:title',
            loadChildren: () =>
              import('./b1-list-recipe/b1-list-recipe.module').then(
                (m) => m.B1ListRecipeModule
              ),
          },                  
        ],
      },
    ]),
  ],
  providers: [],
  entryComponents: [],
})
export class ContentModule {}
