import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/common/api-service/api.service';
import { SEOService } from 'src/app/common/api-service/seo.service';
// import $ from "jquery";
@Component({
  selector: 'app-b1-list-recipe',
  templateUrl: './b1-list-recipe.component.html',
  styleUrls: ['./b1-list-recipe.component.scss']
})
export class B1ListRecipeComponent implements OnInit, OnDestroy {

  subscription: Subscription[] = [];

  //idRecipeCategories
  idRecipeCategories: string = '1';

  //RecipeCategories
  RecipeCategories: any[] = [];
  CookingDifficulty: any[] = [];
  BlogFist: any = {
    id: '',
    IdBlogCategories: '',
    Title: '',
    Thumbnail: '',
    Description: '',
    Content: '',
    NumberView: '',
    CreatedAt: '',
    UpdatedAt: '',
    titleShortBlog: ''
  };
  Blogs: any[] = [];
  isBlogs: boolean = false;
  isBlogFist: boolean = false;

  RecipeView: any[] = [];

  // binding keyword
  keyword: string = '';

  // pagination
  page: number;
  limit: number = 5;
  count: number;
  numberPages: number;
  limitPage = 5;
  limitPages: any;

  //data Recipe
  Recipe: any[] = [];
  defaultImage: any = "assets/images/loading.gif";

  // idCateRecipe: any[] = []

  //urlSEO
  urlSEO: string = '';

  // url Current
  urlCurrent: string = '';

  constructor(private api: ApiService,
    private activatedRoute: ActivatedRoute, private router: Router, private seoService: SEOService) { }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.api.scrollToTop();

    this.getLoadDataListRecipeCategories();
    this.getLoadDataListCookingDifficulty();
    this.getLoadDataBlogs();
    this.getLoadDataRecipeView();

    this.activatedRoute.params.subscribe(params => {
      this.idRecipeCategories = params['idcate'];

      //reset page click
      this.page = 1;
      this.getLoadDataRecipe();
    });
  }

  /**
  * ngOnDestroy
  */
  ngOnDestroy() {
    this.api.scrollToTop();

    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
   * getLoadDataListRecipeCategories
   */
  getLoadDataListRecipeCategories() {
    const param = {}
    this.subscription.push(this.api.excuteAllByWhat(param, '2300', true).subscribe((result) => {
      let data = result.data;
      if (result.status == true && data.length > 0) {
        this.RecipeCategories = data;
      } else {
        this.RecipeCategories = [];
      }
    })
    );
  }

  /**
   * get name display RecipeCategories by id
   */
  getDisplayRecipecategoriesById(id) {
    return this.RecipeCategories.filter((e) => e.id == id)[0]?.Name;
  }

  /**
   * getLoadDataListCookingDifficulty
   */
  getLoadDataListCookingDifficulty() {
    const param = {
    }
    this.subscription.push(
      this.api.excuteAllByWhat(param, '2400', true).subscribe((result) => {
        let data = result.data;
        if (result.status == true && data.length > 0) {
          this.CookingDifficulty = data;
        } else {
          this.CookingDifficulty = [];
        }
      })
    );
  }

  /**
   * get name display CookingDifficulty by id
   */
  getDisplayCookingdifficultyById(id) {
    return this.CookingDifficulty.filter((e) => e.id == id)[0]?.Name;
  }

  /**
   * getLoadDataListRecipeCategories
   */
  getLoadDataBlogs() {
    const param = {
    }
    this.subscription.push(
      this.api.excuteAllByWhat(param, '1609', true).subscribe((result) => {
        let data = result.data;
        if (result.status == true && data.length > 0) {
          this.isBlogFist = true;
          this.BlogFist = data[0];
          //slice Content
          if (this.BlogFist.Content.length > 165) {

            this.BlogFist.contentShortBlog = this.BlogFist.Content.slice(0, 165) + '...';
          } else {
            this.BlogFist.contentShortBlog = this.BlogFist.Content;
          }

          //slice Content
          if (this.BlogFist.Title.length > 55) {

            this.BlogFist.titleShortBlog = this.BlogFist.Title.slice(0, 55) + '...';
          } else {
            this.BlogFist.titleShortBlog = this.BlogFist.Title;
          }

          if (data.length >= 3) {
            this.isBlogs = true;
            this.Blogs.push(data[1]);
            this.Blogs.push(data[2]);

            this.Blogs.forEach(item => {
              //slice Title
              if (item.Title.length > 35) {
                item.titleShort = item.Title.slice(0, 35) + '...';
              } else {
                item.titleShort = item.Title;
              }
            })
          } else {
            this.isBlogs = false;
            this.Blogs = [];
          }

        } else {
          this.BlogFist = [];
          this.isBlogFist = false;
          this.Blogs = [];
        }

      })
    );
  }

  /**
   * getLoadDataRecipeView
   */
  getLoadDataRecipeView() {
    const param = {
    }
    this.subscription.push(
      this.api.excuteAllByWhat(param, '2504', true).subscribe((result) => {
        let data = result.data;
        if (result.status == true && data.length > 0) {
          data.forEach(item => {
            //slice Title
            if (item.Title.length > 35) {
              item.titleShort = item.Title.slice(0, 35) + '...';
            } else {
              item.titleShort = item.Title;
            }
          })
          this.RecipeView = data;
        } else {
          this.RecipeView = [];
        }
      })
    );
  }

  /**
   * getLoadDataRecipe
   */
  getLoadDataRecipe() {
    const param = {
      'IdRecipeCategories': this.idRecipeCategories,
      'keyword': this.api.cleanAccents(this.keyword).trim(),
      'offset': (this.page - 1) * this.limit,
      'limit': this.limit
    }

    this.subscription.push(
      this.api.excuteAllByWhat(param, '2501', true).subscribe((result) => {
        let data = result.data;
        if (result.status == true && data.length > 0) {
          data.forEach(item => {
            //slice Desc
            if (item.Description.length > 150) {
              item.descriptionShort = item.Description.slice(0, 150) + '...';
            } else {
              item.descriptionShort = item.Description;
            }

            //slice Title
            if (item.Title.length > 85) {
              item.titleShort = item.Title.slice(0, 85) + '...';
            } else {
              item.titleShort = item.Title;
            }
          })
          this.Recipe = data;

          // url Current
          this.urlCurrent = this.api.getUrl();

          let title = '';
          if (this.urlCurrent.indexOf('/en/') == -1) {
            title = 'Danh sách công thức';
          } else {
            title = 'List-recipe'
          }

          //Seo title
          this.seoService.setTitle(title);

        } else {
          this.Recipe = [];
        }
      })
    );
    this.countNumberOfPageSearchResult();
  }

  /**
   * countNumberOfPageSearchResult
   */
  countNumberOfPageSearchResult() {
    this.count = 0;
    this.numberPages = 0;

    const param = {
      'IdRecipeCategories': this.idRecipeCategories,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '2502', true).subscribe(result => {
      let data = result.data;
      if (result.status == true && data.length > 0) {
        this.count = data[0].count;
        if (this.count % this.limit == 0) {
          this.numberPages = this.count / this.limit;
          this.limitPagesPagination(this.page);
        }
        else {
          this.numberPages = Math.floor(this.count / this.limit) + 1;
          this.limitPagesPagination(this.page);
        }
      }
    }));
  }

  /**
   * onChangeAfterPageClick
   * @param curpage
   */
  onChangeAfterPageClick(curpage) {
    if (curpage < (this.limitPage * Math.floor(this.numberPages / this.limitPage))) {
      if (curpage == 1) {
        this.page = curpage + this.limitPage;
      }
      else
        if (curpage % this.limitPage != 0) {
          if (this.numberPages % this.limitPage == 0 && curpage > (this.numberPages - this.limitPage)) {
            this.page = this.page
          }
          else {
            this.page = Math.floor(curpage / this.limitPage) * this.limitPage + this.limitPage + 1;
          }
        }
        else
          if (curpage % this.limitPage == 0) {
            this.page = curpage + 1;
          }
    }
    else {
      this.page = this.numberPages;
    }

    this.getLoadDataRecipe();

    this.api.scrollToTopPosition(0, 200);
  }

  /**
  * onChangeBeforePageClick
  * @param curpage
  */
  onChangeBeforePageClick(curpage) {
    if (curpage > this.limitPage) {
      if (curpage % this.limitPage != 0) {
        this.page = Math.floor(curpage / this.limitPage) * this.limitPage - this.limitPage + 1;
      }
      else
        if (curpage % this.limitPage == 0) {
          this.page = curpage - (2 * this.limitPage) + 1;
        }
    }
    else {
      this.page = 1;
    }
    this.getLoadDataRecipe();

    this.api.scrollToTopPosition(0, 200);
  }

  /**
   * limitPagesPagination
   * @param curpage
   */
  limitPagesPagination(curpage) {
    this.limitPages = [];
    if (curpage <= (Math.floor(this.numberPages / this.limitPage) * this.limitPage)) {
      for (var i = 0; i < this.limitPage; i++) {
        if (curpage % this.limitPage == 0) {
          this.limitPages[i] = i + curpage - this.limitPage + 1;
        }
        else {
          this.limitPages[i] = i + Math.floor(curpage / this.limitPage) * this.limitPage + 1;
        }
      }
    }
    else {
      for (var i = 0; i < (this.numberPages - Math.floor(this.numberPages / this.limitPage) * this.limitPage); i++) {
        if (curpage % this.limitPage == 0) {
          this.limitPages[i] = i + curpage - this.limitPage + 1;
        }
        else {
          this.limitPages[i] = i + Math.floor(curpage / this.limitPage) * this.limitPage + 1;
        }
      }
    }
  }

  /**
   * change Page
   * @param page
   */
  changePage(page) {
    this.page = page;
    this.getLoadDataRecipe();
    this.api.scrollToTopPosition(0, 200);
  }

  /**
   * onRecipeClick
   * @param id
   */
  onRecipeClick(id, title) {
    let formatTitle = this.api.cleanAccents(title).split(' ').join('-');
    let re = /\//gi;
    formatTitle = formatTitle.replace(re, '');

    const url = '/b2-detail-recipe/' + id + '/' + formatTitle;
    this.router.navigate([url.toLowerCase()]);
  }

  /**
   * onBlogClick
   * @param id
   */
  onBlogClick(id, title) {
    let formatTitle = this.api.cleanAccents(title).split(' ').join('-');
    let re = /\//gi;
    formatTitle = formatTitle.replace(re, '');

    const url = '/e3-detail-blog/' + id + '/' + formatTitle;
    this.router.navigate([url.toLowerCase()]);
  }

  /**
   * onSearchClick
   */
  onSearchClick() {
    const url = '/m1-search-recipe/' + this.idRecipeCategories + '/' + this.keyword;
    this.router.navigate([url.toLowerCase()]);
  }
  toheader(){
    document.getElementById("header").scrollIntoView({ behavior: 'auto'});
  }
}
