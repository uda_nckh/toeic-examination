import { TransferHttpCacheModule } from '@nguniversal/common';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { B1ListRecipeComponent } from './b1-list-recipe.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { LazyLoadImageModule } from 'ng-lazyload-image'; 

@NgModule({
  declarations: [B1ListRecipeComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: B1ListRecipeComponent,
        children: []
      }
    ]),
    FormsModule,
    ReactiveFormsModule,
    LazyLoadImageModule
  ]
})
export class B1ListRecipeModule { }
