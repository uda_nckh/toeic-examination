import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { SEOService } from 'src/app/common/api-service/seo.service';

@Component({
  selector: 'app-a1-homepgae',
  templateUrl: './a1-homepage.component.html',
  styleUrls: ['./a1-homepage.component.scss']
})
export class A1HomePageComponent implements OnInit, OnDestroy {
  day_title='5/2021';
    news=[
      {
        title:'Thông báo',
        day:'23/12/2020',
        content:'Thông báo về chương trình học bổng khuyến học, khuyến tài do Ông Văn Phú Chính tài trợ'
      },
      {
        title:'Lịch thi và danh sách phòng thi Chuẩn đầu ra Ngoại ngữ',
        day:'16/12/2020',
        content:'Đợt thi tháng 12/2020'
      },
      {
        title:'Lịch thi KTHP Tiếng Anh',
        day:'14/12/2020',
        content:'Các lớp Tiếng Anh không chuyên và học lại yêu cầu'
      },
      {
        title:'Lịch thi KTHP Tiếng Anh',
        day:'14/12/2020',
        content:'Các lớp Tiếng Anh không chuyên và học lại yêu cầu'
      },
      {
        title:'Lịch thi KTHP Tiếng Anh',
        day:'14/12/2020',
        content:'Các lớp Tiếng Anh không chuyên và học lại yêu cầu'
      }
    ]
  //get data community
  community: any;
  communityFeatuded: any;

  communityCategories: any[] = [];

  idcategories: any;

  communityID = {
    id: '',
  };

  //subscription
  subscription: Subscription[] = [];

  // pagination
  page: number;
  limit: number = 5;
  count: number;
  numberPages: number;
  limitPage = 5;
  limitPages: any;
  isReset: boolean = false;

  //data Community
  CommunityPagination: any[] = []

  //idRecipeCategories
  idCommunityCategories: string = '1';

  firstCategory: string;

  // url Current
  urlCurrent: string = '';
  defaultImage: any = "assets/images/loading.gif";

  constructor(private api: ApiService, private router: Router, private seoService: SEOService) { }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.api.scrollToTop();
    this.getDataCommunityFirst();
    this.getDataCommunityCategories();

    //reset page click
    this.page = 1;
  }
  /**
  * ngOnDestroy
  */
  ngOnDestroy() {
    this.api.scrollToTop();

    this.subscription.forEach(item => {
      item.unsubscribe();
    });
  }

  /**
  * get Data Community Categories
  */
  getDataCommunityCategories() {
    const param = {
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '1800', true).subscribe(result=> {
      let data = result.data;
      if (result.status == true && data.length > 0) {
        this.communityCategories = data;
        this.firstCategory = data[0]['id'];
        this.getLoadDataCommunity(data[0].id, false);
      }
    }));
  }

  /**
   * get Data Community
   */
  getDataCommunity(item) {
    const param = {
      'idCommunity': item,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '1900', true).subscribe(result=> {
      let data = result.data;
      if (result.status == true && data.length > 0) {
        this.community = data;
        data.forEach(item => {
          //slice Title
          if (item.Title.length > 75) {
            item.titleShort = item.Title.slice(0, 75) + '...';
          } else {
            item.titleShort = item.Title;
          }

          //slice desc
          if (item.Description.length > 325) {
            item.descShort = item.Description.slice(0, 325) + '...';
          } else {
            item.descShort = item.Description;
          }
        })
      } else {
        this.community = [];
      }
    }));
  }

  /**
  * getDataCommunityFirst
  */
  getDataCommunityFirst() {
    const param = {
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '1901', true).subscribe(result=> {
      let data = result.data;
      if (result.status == true && data.length > 0) {
        data.forEach(item => {
          //slice Title
          if (item.Title.length > 35) {
            item.titleShort = item.Title.slice(0, 35) + '...';
          } else {
            item.titleShort = item.Title;
          }

           // check image invalid
           if (item.Thumbnail == '' || item.Thumbnail == null) {
            item.Thumbnail = 'assets/images/no-image.png';
          }

        });

        this.communityFeatuded = data;


        // url Current
        this.urlCurrent = this.api.getUrl();
        let title = '';
        if (this.urlCurrent.indexOf('/en/') == -1) {
          title = 'Trang chủ';
        } else {
          title = 'List post community'
        }

        //Seo title
        this.seoService.setTitle(title);
      }
    }));
  }

  /**
   * getDisplayCommunityCateById
   */
  getDisplayCommunityCateById(id) {
    return this.communityCategories.filter((e) => e.id == id)[0]?.Name;
  }

  /**
 * onDetailcommunity Click
 * @param idcompany
 */
  onDetailCommunityClick(idcommunity, title) {
    let formatTitle = this.api.cleanAccents(title).split(' ').join('-');
    let re = /\//gi;
    formatTitle = formatTitle.replace(re, '');

    const url = '/f2-detail-community/' + idcommunity + '/' + formatTitle;
    this.router.navigate([url.toLowerCase()]);
  }

  //pagination

  /**
  * getLoadDataCommunity
  */
  getLoadDataCommunity(item, isReset) {
    if (isReset == true) {
      //reset page click
      this.page = 1;
    }
    this.communityID.id = item;

    const param = {
      'IdCommunityCategories': item,
      'offset': (this.page - 1) * this.limit,
      'limit': this.limit
    }

    this.subscription.push(
      this.api.excuteAllByWhat(param, '1903', true).subscribe((result) => {
        let data = result.data;
        if (result.status == true && data.length > 0) {
          data.forEach(item => {
            //slice Title
            if (item.Title.length > 75) {
              item.titleShort = item.Title.slice(0, 75) + '...';
            } else {
              item.titleShort = item.Title;
            }

            //slice desc
            if (item.Description.length > 325) {
              item.descShort = item.Description.slice(0, 325) + '...';
            } else {
              item.descShort = item.Description;
            }

            // check image invalid
            if (item.Thumbnail == '' || item.Thumbnail == null) {
              item.Thumbnail = 'assets/images/no-image.png';
            }
          })
          this.CommunityPagination = data;

        } else {
          this.CommunityPagination = [];
        }
      })
    );
    this.countNumberOfPageSearchResult(item);
  }


  /**
  * countNumberOfPageSearchResult
  */
  countNumberOfPageSearchResult(item) {
    this.count = 0;
    this.numberPages = 0;

    const param = {
      'IdCommunityCategories': item,
    };
    this.subscription.push(this.api.excuteAllByWhat(param, '1904', true).subscribe(result=> {
      let data = result.data;

      if (result.status == true && data.length > 0) {
        this.count = data[0].count;
        if (this.count % this.limit == 0) {
          this.numberPages = this.count / this.limit;
          this.limitPagesPagination(this.page);
        }
        else {
          this.numberPages = Math.floor(this.count / this.limit) + 1;
          this.limitPagesPagination(this.page);
        }
      }

    }));
  }

  /**
   * change Page
   * @param page
   */
  changePage(page, item) {
    this.page = page;
    this.getLoadDataCommunity(item, false);
    this.api.scrollToTopPosition(0, 200);
  }

  /**
  * onChangeBeforePageClick
  * @param curpage
  */
  onChangeBeforePageClick(curpage, item) {
    if (curpage > this.limitPage) {
      if (curpage % this.limitPage != 0) {
        this.page = Math.floor(curpage / this.limitPage) * this.limitPage - this.limitPage + 1;
      }
      else
        if (curpage % this.limitPage == 0) {
          this.page = curpage - (2 * this.limitPage) + 1;
        }
    }
    else {
      this.page = 1;
    }
    this.getLoadDataCommunity(item, false);
    this.api.scrollToTopPosition(0, 200);
  }

  /**
   * onChangeAfterPageClick
   * @param curpage
   */
  onChangeAfterPageClick(curpage, item) {
    if (curpage < (this.limitPage * Math.floor(this.numberPages / this.limitPage))) {
      if (curpage == 1) {
        this.page = curpage + this.limitPage;
      }
      else
        if (curpage % this.limitPage != 0) {
          if (this.numberPages % this.limitPage == 0 && curpage > (this.numberPages - this.limitPage)) {
            this.page = this.page
          }
          else {
            this.page = Math.floor(curpage / this.limitPage) * this.limitPage + this.limitPage + 1;
          }
        }
        else
          if (curpage % this.limitPage == 0) {
            this.page = curpage + 1;
          }
    }
    else {
      this.page = this.numberPages;
    }

    this.getLoadDataCommunity(item, false);
    this.api.scrollToTopPosition(0, 200);
  }


  /**
   * limitPagesPagination
   * @param curpage
   */
  limitPagesPagination(curpage) {
    this.limitPages = [];
    if (curpage <= (Math.floor(this.numberPages / this.limitPage) * this.limitPage)) {
      for (var i = 0; i < this.limitPage; i++) {
        if (curpage % this.limitPage == 0) {
          this.limitPages[i] = i + curpage - this.limitPage + 1;
        }
        else {
          this.limitPages[i] = i + Math.floor(curpage / this.limitPage) * this.limitPage + 1;
        }
      }
    }
    else {
      for (var i = 0; i < (this.numberPages - Math.floor(this.numberPages / this.limitPage) * this.limitPage); i++) {
        if (curpage % this.limitPage == 0) {
          this.limitPages[i] = i + curpage - this.limitPage + 1;
        }
        else {
          this.limitPages[i] = i + Math.floor(curpage / this.limitPage) * this.limitPage + 1;
        }
      }
    }
  }

}
