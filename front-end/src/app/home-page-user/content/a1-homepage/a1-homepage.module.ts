import { TransferHttpCacheModule } from '@nguniversal/common';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { A1HomePageComponent } from './a1-homepage.component';
import { RouterModule } from '@angular/router';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [A1HomePageComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    LazyLoadImageModule,
    RouterModule.forChild([
      {
        path: '',
        component: A1HomePageComponent,
        children: []
      }
    ]),
  ]
})
export class A1HomePageModule { }
