from typing import List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from api.categories import crud, schema
from api.utils import jwtUtil
from api.utils.jwtUtil import get_db
from api.auth import schema as schema_auth

router = APIRouter()


@router.post("/categories", response_model=schema.Category)
def create_category(category: schema.CategoryCreate, db: Session = Depends(get_db)):
    check_name = crud.getname_category(db, name=category.name)
    if check_name:
        raise HTTPException(status_code=404, detail="Name Category already exists")
    return crud.create_category(db=db, category=category)


@router.get("/categories/", response_model=List[schema.Category])
def read_categories(db: Session = Depends(get_db)):
    categories = crud.get_categories(db)
    return categories


@router.get("/categories/{category_id}", response_model=schema.Category)
def read_exam(category_id: int, db: Session = Depends(get_db)):
    db_category = crud.get_category(db, category_id=category_id)
    if db_category is None:
        raise HTTPException(status_code=404, detail="Category not found")
    return db_category


@router.delete('/delete_category')
def delete_category(id: int, db: Session = Depends(get_db)):
    db_category = crud.get_category(db, category_id=id)
    if db_category is None:
        raise HTTPException(status_code=404, detail="category not found")
    try:
        crud.delete_category(db=db, id=id)
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Unable to delete: {e}")
    return {"delete status": "success"}


@router.put('/update_category', response_model=schema.Category)
def update_category(id: int, update_category: schema.CategoryUpdate, db: Session = Depends(get_db)):
    db_category = crud.get_category(db, category_id=id)
    if not db_category:
        raise HTTPException(status_code=404, detail=f"category not found to update")

    return crud.update_category(db=db, categoryexams=update_category, id=id)
