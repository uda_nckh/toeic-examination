from sqlalchemy.orm import Session
from api import models
from api.categories import schema


def create_category(db: Session, category: schema.CategoryCreate):
    db_category = models.CategoryExam(name=category.name)
    db.add(db_category)
    db.commit()
    db.refresh(db_category)
    return db_category


def get_category(db: Session, category_id: int):
    return db.query(models.CategoryExam).filter(models.CategoryExam.id == category_id).first()


def getname_category(db: Session, name: str):
    return db.query(models.CategoryExam).filter(models.CategoryExam.name == name).first()


def get_categories(db: Session):
    return db.query(models.CategoryExam).all()


def delete_category(db: Session, id: int):
    try:
        db.query(models.CategoryExam).filter(models.CategoryExam.id == id).delete()
        db.execute("SET @num :=0;UPDATE categoryexams SET id = @num := (@num+1);ALTER TABLE categoryexams AUTO_INCREMENT = 1;")
        db.commit()
    except Exception as e:
        raise Exception(e)


def update_category(db: Session, id: int, categoryexams: schema.CategoryUpdate):
    db.query(models.CategoryExam).filter(models.CategoryExam.id == id).update(vars(categoryexams))
    db.commit()
    return db.query(models.CategoryExam).filter(models.CategoryExam.id == id).first()
