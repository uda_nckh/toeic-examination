from pydantic import BaseModel
from datetime import datetime
from typing import Optional


class CategoryBase(BaseModel):
    name: str

class CategoryCreate(CategoryBase):
    pass

class Category(CategoryBase):
    id: int
    created: Optional[datetime] = None
    updated: Optional[datetime] = None
    class Config:
        orm_mode = True

class CategoryUpdate(BaseModel):
    name: str

    class Config:
        orm_mode = True
