from datetime import datetime
from typing import Optional
from pydantic import BaseModel


class UserList(BaseModel):
    username: str
    email: str
    password: str
    id: int = None
    create_on: Optional[datetime] = None
    is_active: bool = None

class UserCreate(UserList):
    pass

class ForgotPassword(BaseModel):
    email: str

class ResetPassword(BaseModel):
    reset_password_token: str
    new_password: str
    confirm_password: str

class Token(BaseModel):
    access_token: str
    token_type: str
    expired_in: str
    user_info: UserList

class TokenData(BaseModel):
    username: str = None