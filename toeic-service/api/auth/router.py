import uuid
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer
from api.auth import crud
from api.auth import schema
from api.exceptions.business import BusinessException
from api.utils import cryptoUtil, jwtUtil, constantUtil


router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/login")

@router.post("/auth/register", response_model=schema.UserList)
async def register(user: schema.UserCreate):
    # Check user exits
    result = await crud.find_exits_user(user.email)
    if result:
        raise BusinessException(status_code=status.HTTP_409_CONFLICT, detail="Email already registered.")
    # Create new user
    user.password = cryptoUtil.hash_password(user.password)
    await crud.save_user(user)
    return {**user.dict()}

@router.post("/auth/login",response_model=schema.Token)
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    # Check user exits
    result = await crud.find_exits_user(form_data.username)
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")

    #Verify password
    user = schema.UserCreate(**result)
    verify_password = cryptoUtil.verify_password(form_data.password,user.password)
    if not verify_password:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect username or password.")

    #Create token
    access_token_expires = jwtUtil.timedelta(minutes=constantUtil.ACCESS_TOKEN_EXPIRE_MINUTE)
    acces_token = await jwtUtil.create_access_token(
        data={"sub": form_data.username},
        expires_delta = access_token_expires,
    )
    # print("debug acces token:",acces_token)
    results = {
        "access_token": acces_token,
        "token_type": "bearer",
        "expired_in": constantUtil.ACCESS_TOKEN_EXPIRE_MINUTE * 60,
        "user_info": user
    }

    return results


@router.post("/auth/forgot-password")
async def forgot_password(request: schema.ForgotPassword):
    # Check user exits
    result = await crud.find_exits_user(request.email)
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found.")

    #Create reset code and save in database
    reset_code = str(uuid.uuid1())
    await crud.create_reset_code(request.email,reset_code)

    #Sending Email
    subject = "Hello Coder"
    recipient = [request.email]
    message = """
    <!DOCTYPE html>
    <html>
    <title>Reset Password</title>
    <body>
    <div style="width:100%";font-family: monospace;">
        <h1>Hello, {0:}</h1>
<p>Someone has requested a link to reset your password. If requested this, you can change your password through the button</p>
        <a href="http://127.0.0.1:8000/user/forgot-password?reset_password_token={1:}" style="box-sizing:border-box;border-color:cyan;">
        <p>If you didn't request this, you can ignore this email</p>
        <p>Your password won't change  until you access the link above and create a new one.</p>
    </div>
    </body>
    </html>
    """.format(request.email,reset_code)
    # await emailUtil.send_email(subject, recipient, message)
    return {
        "reset_code": reset_code,
        "code":200,
        "message": "We've sent an email with instructions to reset your password"
    }

@router.patch("/auth/reset-password")
async def reset_password(request: schema.ResetPassword):
    # Check valid reset password token
    reset_token = await crud.check_reset_password_token(request.reset_password_token)
    if not reset_token:
        raise HTTPException(status_code=404, detail="Reset password token has expired, please request a new one.")

    # Check both new  & confirm password are matched
    if request.new_password != request.confirm_password:
        raise HTTPException(status_code=404, detail="New password is not match.")

    # Reset new password
    forgot_password_object = schema.ForgotPassword(**reset_token)
    new_hash_password = cryptoUtil.hash_password(request.new_password)
    await crud.reset_password(new_hash_password, forgot_password_object.email)

    # Disable reset code
    await crud.disable_reset_code(request.reset_password_token, forgot_password_object.email)
    return {
        "code": 200,
        "message": "Password has been reset successfully."
    }