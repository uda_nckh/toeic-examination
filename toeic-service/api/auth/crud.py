from sqlalchemy.orm import Session
from api import models
from api.auth import schema
from api.utils.dbUtil import database


def find_exits_user(email: str):
    query = "SELECT * FROM users WHERE email=:email AND is_active=1"
    return database.fetch_one(query, values={"email": email})


def find_black_list_token(token: str):
    query = "SELECT * FROM blacklist WHERE token =:token"
    return database.fetch_one(query, values={"token": token})


def save_user(users: schema.UserCreate):
    query = "INSERT INTO users VALUES (NULL, :username, :email, :password, CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','+07:00'),1)"
    return database.execute(query, values={"username": users.username,"email": users.email, "password": users.password})

def create_reset_code(email: str, reset_code: str):
    query = "INSERT INTO codes VALUES (NULL , :email, :reset_code, CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','+07:00'),'1')"
    return database.execute(query, values={"email": email, "reset_code": reset_code})

def check_reset_password_token(reset_password_token: str):
    query = "SELECT * FROM codes WHERE status='1' AND reset_code=:reset_password_token AND expired_in >= CONVERT_TZ(UTC_TIMESTAMP(),'+00:00','+07:00') - interval 1 minute"
    return database.fetch_one(query, values={"reset_password_token": reset_password_token})

def reset_password(new_hash_password: str, email: str):
    query = "UPDATE users SET password=:password WHERE email=:email"
    return database.execute(query,values={"password":new_hash_password,"email": email})

def disable_reset_code(reset_password_token: str, email: str):
    query = "UPDATE codes SET status='0' WHERE status='1' AND reset_code=:reset_code AND email=:email"
    return database.execute(query, values={"reset_code": reset_password_token,"email": email})

