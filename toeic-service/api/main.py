from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from starlette.middleware.cors import CORSMiddleware

from api import models
from api.auth import router as auth_router
from api.users import router as user_router
from api.categories import router as category_router
from api.exams import router as exam_router
from api.questions import router as question_router
from api.exceptions.business import BusinessException
from api.utils.dbUtil import database, engine

app = FastAPI(
    docs_url="/docs",
    redoc_url="/redocs",
    title="FastAPI (Python)",
    description="FastAPI Framwork",
    version="1.0",
    openapi_url="/openapi.json"
)
# models.Base.metadata.drop_all(bind=engine)
models.Base.metadata.create_all(bind=engine)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
    allow_credentials=True,
)

@app.on_event("startup")
async def startup():
    print("startup")
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    print("shutdown")
    await database.disconnect()

@app.exception_handler(BusinessException)
async def unicorn_exception_handler(request: Request, e: BusinessException):
    return JSONResponse(
        status_code=418,
        content={"code": e.status_code, "message": e.detail},
    )


app.include_router(auth_router.router, tags=["Auth"])
app.include_router(user_router.router, tags=["Users"])
app.include_router(category_router.router,tags=["Categories Exam"])
app.include_router(exam_router.router,tags=["Exam"])
app.include_router(question_router.router, tags=["Question"])

# import shutil
# from typing import List
# import os
#
# @app.post("/images")
# async def image(images: List[UploadFile] = File(...)):
#     for image in images:
#         with open(image.filename, "wb") as buffer:
#             shutil.copyfileobj(image.file, buffer)
#
#         return {"filename": image.filename}
#
# ALLOWED_EXTENSIONS = set(['csv','jpg','png'])
# # UPLOAD_FOLDER = "api/data/images"
#
#
# def allowed_file(filename):
#     return '.' in filename and \
#         filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
#
# @app.post("/upload/")
# async def upload(file: UploadFile = File(...)):
#     if file and allowed_file(file.filename):
#         filename = file.filename
#         fileobj = file.file
#         upload_dir = open(os.path.join("api/data/images/%s"%filename),'wb+')
#         shutil.copyfileobj(fileobj, upload_dir)
#         upload_dir.close()
#         return {"filename": filename}
#     if file and not allowed_file(file.filename):
#         return {"warning": "not allowed file"}
