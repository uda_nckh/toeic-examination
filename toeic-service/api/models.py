from datetime import datetime
import pytz
from sqlalchemy import MetaData, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Column
from sqlalchemy.sql import func
from sqlalchemy.types import *
from sqlalchemy_utils import EmailType, URLType
from .utils.dbUtil import Base

metadata = MetaData()

class Users(Base):
        __tablename__ = 'users'
        metadata
        id = Column(INT, primary_key=True, autoincrement=True)
        username = Column(String(100))
        email = Column(EmailType)
        password = Column(String(length=200))
        created = Column(DateTime(timezone=True), server_default=func.now(),
                           default=datetime.now(tz=pytz.timezone('Asia/Ho_Chi_Minh')))
        is_active = Column(Boolean, default=True)

# class Registration(Users):
#     __tablename__ = 'registration'
#     metadata
#     id = Column(INT, primary_key=True, autoincrement=True)
#     birthday = Column(Date)
#     sex = Column(String(10))
#     phonenumber = Column(Integer)
#     address = Column(String(length=200))
#     cmnd = Column(Integer)
#     avatar = Column(URLType)


class Codes(Base):
    __tablename__ = 'codes'
    metadata
    id = Column(INT, primary_key=True, autoincrement=True)
    email = Column(EmailType)
    reset_code = Column(String(length=200), nullable=False)
    expired_in = Column(DateTime(timezone=True), server_default=func.now(),
                        default=datetime.now(tz=pytz.timezone('Asia/Ho_Chi_Minh')))
    status = Column(String(1))

class BlackList(Base):
    __tablename__ = 'blacklist'
    metadata
    token = Column(String(250), primary_key=True)
    email = Column(String(150))

class CategoryExam(Base):
    __tablename__ = 'categoryexams'
    metadata
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(200))
    created = Column(DateTime(timezone=True), server_default=func.now(),
                       default=datetime.now(tz=pytz.timezone('Asia/Ho_Chi_Minh')))
    exams = relationship("Exams", back_populates="categories")

class Exams(Base):
    __tablename__ = 'exams'
    metadata
    id = Column(Integer, primary_key=True)
    category_id = Column(Integer, ForeignKey("categoryexams.id"))
    name = Column(String(200))
    created = Column(DateTime(timezone=True), server_default=func.now(),
                       default=datetime.now(tz=pytz.timezone('Asia/Ho_Chi_Minh')))

    categories = relationship("CategoryExam", back_populates="exams")
    questions = relationship("Questions", back_populates="exams")

class Questions(Base):
    __tablename__ = 'questions'
    metadata
    id = Column(Integer, primary_key=True, autoincrement=True)
    exams_id = Column(Integer, ForeignKey("exams.id"))
    question_index = Column(Integer)
    question = Column(String(500))
    anwser_a = Column(String(500))
    anwser_b = Column(String(500))
    anwser_c = Column(String(500))
    anwser_d = Column(String(500))
    correct = Column(String(500))
    image_url = Column(URLType)
    audio = Column(URLType)
    create_on = Column(DateTime(timezone=True), server_default=func.now(),
                       default=datetime.now(tz=pytz.timezone('Asia/Ho_Chi_Minh')))

    exams = relationship("Exams", back_populates="questions")


