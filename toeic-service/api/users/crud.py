from api.users import schema as user_schema
from api.auth import schema as auth_schema
from api.utils.dbUtil import database


def update_user(
    request: user_schema.UserUpdate,
    current_user: auth_schema.UserList
):
    query = "UPDATE users SET username=:username where email=:email"
    return database.execute(query, values={"username": request.username, "email": current_user.email})


def deactivate_user(current_user: auth_schema.UserList):
    query = "UPDATE users SET is_active=2 where is_active=1 and email=:email "
    return database.execute(query, values={"email": current_user.email})


def change_password(
    change_password_object: user_schema.ChangePassword,
    current_user: auth_schema.UserList
):
    query = "UPDATE users SET password=:password where is_active=1 and email=:email "
    return database.execute(query, values={"password": change_password_object.new_password, "email": current_user.email})

def save_black_list_token(
    token: str,
    current_user: auth_schema.UserList
):
    query="INSERT INTO blacklist VALUES (:token, :email)"
    return database.execute(query, values={"token": token, "email":current_user.email})