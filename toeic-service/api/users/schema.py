from pydantic import BaseModel


class UserUpdate(BaseModel):
    username: str


class ChangePassword(BaseModel):
    current_password: str
    new_password: str
    confirm_password: str

