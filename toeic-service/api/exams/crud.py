from sqlalchemy.orm import Session
from api import models
from api.exams import schema


def create_exam(db: Session, exam: schema.ExamCreate):
    db_exam = models.Exams(category_id=exam.category_id, name=exam.name)
    db.add(db_exam)
    db.commit()
    db.refresh(db_exam)
    return db_exam


# def create_question(db: Session, exams_id: int,questions: str,anwser_a: str,anwser_b: str,anwser_c: str,anwser_d: str,correct: str,image_url: str):
#     db_question = models.Questions(exams_id=exams_id,questions=questions,anwser_a=anwser_a,anwser_b=anwser_b,
#                                    anwser_c=anwser_c,anwser_d=anwser_d,correct=correct,image_url=image_url)
#     db.add(db_question)
#     db.commit()
#     db.refresh(db_question)
#     return db_question

def get_exam(db: Session, exam_id: int):
    return db.query(models.Exams).filter(models.Exams.id == exam_id).first()


def getname_exam(db: Session, name: int):
    return db.query(models.Exams).filter(models.Exams.name == name).first()


def get_exams(db: Session):
    return db.query(models.Exams).all()


def delete_exam(db: Session, id: int):
    try:
        db.query(models.Exams).filter(models.Exams.id == id).delete()
        db.commit()
    except Exception as e:
        raise Exception(e)


def update_exam(db: Session, id: int, exams: schema.ExamUpdate):
    db.query(models.Exams).filter(models.Exams.id == id).update(vars(exams))
    db.commit()
    return db.query(models.Exams).filter(models.Exams.id == id).first()
