from pydantic import BaseModel
from datetime import datetime
from typing import Optional


class ExamBase(BaseModel):
    category_id: int
    name: str


class ExamCreate(ExamBase):
    pass


class Exam(ExamBase):
    id: int
    create_on: Optional[datetime] = None

    class Config:
        orm_mode = True


class ExamUpdate(BaseModel):
    name: str

    class Config:
        orm_mode = True
