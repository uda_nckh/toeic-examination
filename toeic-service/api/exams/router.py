import os
import shutil
from typing import List
from fastapi import APIRouter, Depends, HTTPException, UploadFile, File, status
from sqlalchemy.orm import Session
from api.exams import crud, schema
from api.categories import crud as crud_categories
from api.auth import schema as schema_auth
from api.utils import jwtUtil
from api.utils.jwtUtil import get_db

router = APIRouter()


@router.post("/exams", response_model=schema.Exam)
def create_question(exam: schema.ExamCreate, db: Session = Depends(get_db),
                    ):
    check_category = crud_categories.get_category(db, category_id=exam.category_id)
    if check_category is None:
        raise HTTPException(status_code=404, detail="Category not found")
    check_name = crud.getname_exam(db, name=exam.name)
    if check_name:
        raise HTTPException(status_code=404, detail="Name Exam already exists")
    return crud.create_exam(db=db, exam=exam)


@router.get("/exams/", response_model=List[schema.Exam])
def read_exams(db: Session = Depends(get_db)):
    exams = crud.get_exams(db)
    return exams


@router.get("/exams/{exam_id}", response_model=schema.Exam)
def read_exam(exam_id: int, db: Session = Depends(get_db)):
    db_exam = crud.get_exam(db, exam_id=exam_id)
    if db_exam is None:
        raise HTTPException(status_code=404, detail="Exam not found")
    return db_exam


@router.delete('/delete_exam')
def delete_exam(id: int, db: Session = Depends(get_db)):
    db_exam = crud.get_exam(db, exam_id=id)
    if db_exam is None:
        raise HTTPException(status_code=404, detail="Exam not found")
    try:
        crud.delete_exam(db=db, id=id)
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Unable to delete: {e}")
    return {"delete status": "success"}


@router.put('/update_exam', response_model=schema.ExamUpdate)
def update_exam(id: int, update_param: schema.ExamUpdate, db: Session = Depends(get_db)):
    db_exam = crud.get_exam(db, exam_id=id)
    if not db_exam:
        raise HTTPException(status_code=404, detail=f"Exam not found to update")

    return crud.update_exam(db=db, exams=update_param, id=id)
