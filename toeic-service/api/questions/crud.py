from sqlalchemy.orm import Session
from api import models
from api.questions import schema
from typing import Optional
from fastapi import FastAPI, Query


# def create_question(db: Session, questions: schema.QuestionCreate):
#     db_question = models.Questions(exams_id=questions.exams_id,questions=questions.questions,anwser_a=questions.anwser_a,anwser_b=questions.anwser_b,
#                                    anwser_c=questions.anwser_c,anwser_d=questions.anwser_d,correct=questions.correct,image_url=questions.image_url)
#     db.add(db_question)
#     db.commit()
#     db.refresh(db_question)
#     return db_question

def create_question(db: Session, exams_id: int, question_index: int, question: Optional[str] = Query(None),
                    anwser_a: Optional[str] = Query(None), anwser_b: Optional[str] = Query(None),
                    anwser_c: Optional[str] = Query(None),
                    anwser_d: Optional[str] = Query(None), correct: Optional[str] = Query(None),
                    image_url: Optional[str] = Query(None), audio: Optional[str] = Query(None)):
    db_question = models.Questions(exams_id=exams_id,question_index=question_index, question=question, anwser_a=anwser_a, anwser_b=anwser_b,
                                   anwser_c=anwser_c, anwser_d=anwser_d, correct=correct, image_url=image_url,
                                   audio=audio)
    db.add(db_question)
    db.commit()
    db.refresh(db_question)
    return db_question


def get_question(db: Session, question_id: int):
    return db.query(models.Questions).filter(models.Questions.id == question_id).first()


def get_question_ByExamId(db: Session, exam_id: int):
    return db.query(models.Questions).filter(models.Questions.exams_id == exam_id).all()


def get_questions(db: Session):
    return db.query(models.Questions).all()


def delete_question(db: Session, id: int):
    try:
        db.query(models.Questions).filter(models.Questions.id == id).delete()
        db.execute("SET @num :=0;UPDATE questions SET id = @num := (@num+1);ALTER TABLE questions AUTO_INCREMENT = 1;")
        db.commit()
    except Exception as e:
        raise Exception(e)


def update_question(db: Session, id: int, questions: schema.QuestionUpdate):
    db.query(models.Questions).filter(models.Questions.id == id).update(vars(questions))
    db.commit()
    return db.query(models.Questions).filter(models.Questions.id == id).first()
