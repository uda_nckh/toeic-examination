import os
import shutil
from typing import List
from fastapi import APIRouter, Depends, HTTPException, UploadFile, File, status
from sqlalchemy.orm import Session
from api.questions import crud, schema
from api.auth import schema as schema_auth
from api.utils import jwtUtil
from api.utils.jwtUtil import get_db
from api.exams import crud as crud_exam
from typing import Optional
from fastapi import FastAPI, Query
import cloudinary
import cloudinary.uploader

router = APIRouter()

cloudinary.config(
    cloud_name="udacloudi",
    api_key="874396459768637",
    api_secret="HPxxLhtezUB2t_CjfK0BMNnODXw"
)


# @router.post("/questions",response_model=schema.Question)
# def create_question(questions: schema.QuestionCreate, db: Session = Depends(get_db)):
#     return crud.create_question(db=db, questions=questions)

@router.post("/questions/", response_model=schema.Question, status_code=status.HTTP_201_CREATED)
def create_question(exams_id: int,question_index: int, question: Optional[str] = Query(None), anwser_a: Optional[str] = Query(None),
                    anwser_b: Optional[str] = Query(None), anwser_c: Optional[str] = Query(None),
                    anwser_d: Optional[str] = Query(None), correct: Optional[str] = Query(None),
                    db: Session = Depends(get_db), image_file: UploadFile = File(None),
                    audio_file: UploadFile = File(None),
                    ):
    # image_dir =f"api/data/images/exam_{exams_id}"
    # if not os.path.exists(image_dir):
    #     os.makedirs(image_dir)

    if image_file:
        # with open(image_dir + "/" + image_file.filename, "wb") as image:
        #     shutil.copyfileobj(image_file.file, image)
        #
        # image_url = str("../../../../assets/images/exam_%d" % exams_id + "/" + image_file.filename)

        result = cloudinary.uploader.upload(image_file.file, folder="images/")
        image_url = result.get("url")
    else:
        image_url: Optional[str] = Query(None)

    # audio_dir = f"api/data/audio/exam_{exams_id}"
    # if not os.path.exists(audio_dir):
    #     os.makedirs(audio_dir)

    if audio_file:
        # with open(audio_dir + "/" + audio_file.filename, "wb") as image:
        #     shutil.copyfileobj(audio_file.file, image)
        #
        # audio = str("../../../../assets/audio/exam_%d" % exams_id + "/" + audio_file.filename)

        result = cloudinary.uploader.upload(audio_file.file, resource_type="video", folder="audio/")
        audio = result.get("url")
    else:
        audio: Optional[str] = Query(None)

    result = crud_exam.get_exam(db, exam_id=exams_id)
    if result is None:
        raise HTTPException(status_code=404, detail="Exam not found")

    return crud.create_question(db=db, exams_id=exams_id,question_index=question_index, question=question, anwser_a=anwser_a, anwser_b=anwser_b,
                                anwser_c=anwser_c, anwser_d=anwser_d, correct=correct, image_url=image_url, audio=audio)


@router.get("/questions/", response_model=List[schema.Question])
def read_questions(db: Session = Depends(get_db)):
    questions = crud.get_questions(db)
    return questions


@router.get("/questions/{question_id}", response_model=schema.Question)
def read_question(question_id: int, db: Session = Depends(get_db)):
    db_question = crud.get_question(db, question_id=question_id)
    if db_question is None:
        raise HTTPException(status_code=404, detail="Question not found")
    return db_question


@router.get("/questions/{exam_id}/", response_model=List[schema.Question])
def read_question(exam_id: int, db: Session = Depends(get_db)):
    db_question = crud.get_question_ByExamId(db, exam_id=exam_id)
    if db_question is None:
        raise HTTPException(status_code=404, detail="Question not found")
    return db_question


@router.delete('/delete_question')
def read_question(id: int, db: Session = Depends(get_db)):
    db_question = crud.get_question(db, question_id=id)
    if db_question is None:
        raise HTTPException(status_code=404, detail="Question not found")
    try:
        crud.delete_question(db=db, id=id)
    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Unable to delete: {e}")
    return {"delete status": "success"}


@router.put('/update_question', response_model=schema.QuestionUpdate)
def update_question(id: int, update_param: schema.QuestionUpdate, db: Session = Depends(get_db)):
    db_question = crud.get_question(db, question_id=id)
    if not db_question:
        raise HTTPException(status_code=404, detail=f"Question not found to update")

    return crud.update_question(db=db, questions=update_param, id=id)
