from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class QuestionBase(BaseModel):
    exams_id: int
    question_index : int
    question: Optional[str] = None
    anwser_a: Optional[str] = None
    anwser_b: Optional[str] = None
    anwser_c: Optional[str] = None
    anwser_d: Optional[str] = None
    correct: Optional[str] = None
    image_url: Optional[str] = None
    audio: Optional[str] = None


class QuestionCreate(QuestionBase):
    pass


class Question(QuestionBase):
    id: int
    create_on: Optional[datetime] = None

    class Config:
        orm_mode = True


class QuestionUpdate(BaseModel):
    question_index : int
    question: Optional[str] = None
    anwser_a: Optional[str] = None
    anwser_b: Optional[str] = None
    anwser_c: Optional[str] = None
    anwser_d: Optional[str] = None
    correct: Optional[str] = None
    image_url: Optional[str] = None
    audio: Optional[str] = None
