import jwt
from jose import jwt
from jwt import PyJWTError
from pydantic import ValidationError
from datetime import datetime, timedelta
from api.utils import constantUtil
from api.auth import crud, schema
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from typing import Optional
from api.utils.dbUtil import SessionLocal
from sqlalchemy.orm import Session

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="/auth/login"
)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_token_user(token: str = Depends(oauth2_scheme)):
    return token

async def create_access_token(*, data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, constantUtil.SECRET_KEY, algorithm=constantUtil.ALGORITHM_HS256)
    return encoded_jwt


async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    try:
        payload = jwt.decode(token, constantUtil.SECRET_KEY, algorithms=[constantUtil.ALGORITHM_HS256])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception

        #check backlist token
        black_list_token = await crud.find_black_list_token(token)
        if black_list_token:
            raise credentials_exception


        token_data = schema.TokenData(username=username)
    except (PyJWTError, ValidationError):
        raise credentials_exception

    user = await crud.find_exits_user(token_data.username);
    if user is None:
        raise credentials_exception

    return schema.UserList(**user)


def get_current_active_user(current_user: schema.UserList = Depends(get_current_user)):
    if current_user.is_active != 1:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Inactive user")

    return current_user
