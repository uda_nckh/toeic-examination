import databases
import sqlalchemy
from databases import Database
from starlette.config import Config
import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


# 2. Using starlette to load .env configuration file
def database_mysql_url_config():
    conf = Config("api/.env")
    return str(conf("DB_CONNECTION") + "://" + conf("DB_USERNAME") + ":" + conf("DB_PASSWORD") +
               "@" + conf("DB_HOST") + ":" + conf("DB_PORT") + "/" + conf("DB_DATABASE"))


TEST = False
if TEST:
    os.environ["MYSQL_USER"] = 'toeicdev'
    os.environ["MYSQL_PASSWORD"] = 'uda123'
    os.environ["MYSQL_HOST"] = '127.0.0.1'
    os.environ["MYSQL_DATABASE"] = 'toeic'
    os.environ["MYSQL_PORT"] = '3306'
    user_name = os.environ["MYSQL_USER"]
    password = os.environ["MYSQL_PASSWORD"]
    host = os.environ["MYSQL_HOST"]
    database_name = os.environ["MYSQL_DATABASE"]
    database_port = os.environ["MYSQL_PORT"]

    DATABASE_URI = database_mysql_url_config()
else:
    DATABASE_URI = os.getenv('DATABASE_URI')

print("DATABASE_URI:", DATABASE_URI)

engine = create_engine(DATABASE_URI)

database = Database(DATABASE_URI)

SessionLocal = sessionmaker(autocommit=False, autoflush=False,
                            bind=engine)
Base = declarative_base()
