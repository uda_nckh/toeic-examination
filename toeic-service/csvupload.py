import csv
import datetime
from api import models
from sqlalchemy import create_engine
from databases import Database
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


DATABASE_URI = "mysql://toeicdev:uda123@toeic_db:3306/toeic"

print("DATABASE_URI:", DATABASE_URI)

engine = create_engine(DATABASE_URI)

database = Database(DATABASE_URI)

SessionLocal = sessionmaker(autocommit=False, autoflush=False,
                            bind=engine)
Base = declarative_base()

db = SessionLocal()

models.Base.metadata.create_all(bind=engine)

with open(f"api/data/Dethi.csv",encoding = 'utf-8') as f:
    csv_reader = csv.DictReader(f)
    for row in csv_reader:
        now = datetime.datetime.now()
        db_questions = models.Questions(
            exams_id=1,
            question=row["questions"],
            anwser_a=row["answer_a"],
            anwser_b=row["answer_b"],
            anwser_c=row["answer_c"],
            anwser_d=row["answer_d"],
            correct=row["correct"],
            image_url=row["link_img"],
            audio = "audio",
            create_time=now
        )
        db.add(db_questions)

    db.commit()

db.close()
