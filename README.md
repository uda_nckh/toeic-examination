# TOEICUDA
Prerequisites

Before you continue, ensure you meet the following requirements:
* You have installed the version Python 3.6+
* You have installed Docker desktop
* You have installed Mysql
***
Run Back-end 
* From toeic-examination, turn on cmd or teminal and run commands:
* Build and rebuild service: `Docker-compose up -d --no-deps --build toeic_service`
* Start back-end: `Docker-compose up`
* Run and test API on: http://localhost:8001/docs
***
Run Front-end
* From toeic-examination cd front-end, turn on cmd or teminal and run commands:
* Import modul front-end: `npm install`
* Start front-end: `npm start`
* Run front-end on: http://localhost:4200